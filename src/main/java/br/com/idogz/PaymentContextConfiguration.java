package br.com.idogz;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.idogz.adapter.controller.payment.PaymentController;
import br.com.idogz.adapter.controller.payment.impl.PaymentControllerImpl;
import br.com.idogz.adapter.controller.webhook.WebhookController;
import br.com.idogz.adapter.controller.webhook.impl.IdogzPagoWebhookControllerImpl;
import br.com.idogz.adapter.controller.webhook.impl.MercadoPagoWebhookControllerImpl;
import br.com.idogz.adapter.gateway.order.impl.UpdateOrderStatusPaymentDataGatewayImpl;
import br.com.idogz.adapter.gateway.payment.impl.FindPaymentOrderDataGatewayImpl;
import br.com.idogz.adapter.presenter.payment.PaymentPresenter;
import br.com.idogz.adapter.repository.order.OrderRepository;
import br.com.idogz.adapter.service.FindPaymentOrderService;
import br.com.idogz.domain.gateway.order.FindOrderByIdDataGateway;
import br.com.idogz.domain.gateway.order.FindOrderByIdPaymentDataGateway;
import br.com.idogz.domain.gateway.order.UpdateOrderStatusPaymentDataGateway;
import br.com.idogz.domain.gateway.payment.FindPaymentOrderDataGateway;
import br.com.idogz.domain.usecase.payment.RegisterPaymentUseCaseInteractor;
import br.com.idogz.domain.usecase.payment.VerifyStatusPaymentOrderUserCaseInteractor;
import br.com.idogz.domain.usecase.payment.impl.RegisterPaymentUseCaseInteractorImpl;
import br.com.idogz.domain.usecase.payment.impl.VerifyStatusPaymentOrderImpl;

@Configuration
public class PaymentContextConfiguration {

	@Bean
	VerifyStatusPaymentOrderUserCaseInteractor verifyStatusPaymentOrderUserCaseInteractor(
			final FindOrderByIdDataGateway gateway) {
		return new VerifyStatusPaymentOrderImpl(gateway);
	}

	@Bean
	RegisterPaymentUseCaseInteractor registerPaymentUseCaseInteractor(
			final FindOrderByIdPaymentDataGateway findOrderByIdPaymentDataGateway,
			final FindPaymentOrderDataGateway findPaymentOrderDataGateway,
			final UpdateOrderStatusPaymentDataGateway updateOrderStatusPaymentDataGateway) {
		return new RegisterPaymentUseCaseInteractorImpl(findOrderByIdPaymentDataGateway, findPaymentOrderDataGateway,
				updateOrderStatusPaymentDataGateway);
	}

	@Bean
	FindPaymentOrderDataGateway findPaymentOrderDataGateway(final FindPaymentOrderService service) {
		return new FindPaymentOrderDataGatewayImpl(service);
	}

	@Bean
	UpdateOrderStatusPaymentDataGateway updateOrderStatusPaymentDataGateway(final OrderRepository repository) {
		return new UpdateOrderStatusPaymentDataGatewayImpl(repository);
	}

	@Bean
	PaymentController paymentController(
			final VerifyStatusPaymentOrderUserCaseInteractor verifyStatusPaymentOrderUserCaseInteractor,
			final PaymentPresenter presenter) {
		return new PaymentControllerImpl(verifyStatusPaymentOrderUserCaseInteractor, presenter);
	}

	@Bean
	WebhookController mercadoPagoWebhookController(final RegisterPaymentUseCaseInteractor useCase,
			final FindPaymentOrderDataGateway paymentOrderDataGateway,
			final FindOrderByIdPaymentDataGateway findOrderByIdPaymentDataGateway,
			final ApiMercadoPagoPropertiesFactory propertiesMercadoPago) {
		return new MercadoPagoWebhookControllerImpl(useCase, paymentOrderDataGateway, findOrderByIdPaymentDataGateway,
				propertiesMercadoPago);
	}

	@Bean
	WebhookController idogzPagoWebhookController(final RegisterPaymentUseCaseInteractor useCase,
			final FindPaymentOrderDataGateway paymentOrderDataGateway,
			final FindOrderByIdPaymentDataGateway findOrderByIdPaymentDataGateway) {
		return new IdogzPagoWebhookControllerImpl(useCase, findOrderByIdPaymentDataGateway);
	}

}
