package br.com.idogz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info=@Info(title="IDogz API v1", version = "1.2.1"))
public class IdogzApplication {

	public static void main(final String[] args) {
		SpringApplication.run(IdogzApplication.class, args);
	}

}
