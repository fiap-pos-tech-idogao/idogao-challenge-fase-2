package br.com.idogz.adapter.controller.product;

import br.com.idogz.domain.exception.DomainException;

public interface ProductController {

	Object create(CreateProductDto createProductDto) throws DomainException;

	void update(UpdateProductDto updateProductDto) throws DomainException;

	void remove(int id) throws DomainException;

	Object findProductByCategory(Integer id) throws DomainException;
}