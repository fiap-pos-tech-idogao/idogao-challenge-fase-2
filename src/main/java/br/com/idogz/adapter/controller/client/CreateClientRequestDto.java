package br.com.idogz.adapter.controller.client;

public record CreateClientRequestDto(Integer id, String name, String cpf, String email) {

}
