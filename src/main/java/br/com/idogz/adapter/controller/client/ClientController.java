package br.com.idogz.adapter.controller.client;

import br.com.idogz.domain.exception.DomainException;

public interface ClientController {

	Object create(final CreateClientRequestDto clientDto) throws DomainException;

	Object findClientWith(final String cpf) throws DomainException;


}