package br.com.idogz.adapter.controller.product;

public record CreateProductDto(Integer id, Integer idCategory, String name, Double price, String description,
		Boolean active, String urlImage) {
}
