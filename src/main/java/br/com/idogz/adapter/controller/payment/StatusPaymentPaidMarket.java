package br.com.idogz.adapter.controller.payment;

import java.util.function.Predicate;
import java.util.stream.Stream;

import br.com.idogz.domain.entity.exception.UnexpectedStatusOrderException;

public enum StatusPaymentPaidMarket {

	PENDING(3, "pending", null), APPROVED(1, "approved", null), CANCELLED(2, "cancelled", null);

	private final int id;
	private final String description;
	private final StatusPaymentPaidMarket oldStatusPaymentPaidMarket;

	StatusPaymentPaidMarket(final int id, final String description, final StatusPaymentPaidMarket oldStatusPaymentPaidMarket) {
		this.id = id;
		this.description = description;
		this.oldStatusPaymentPaidMarket = oldStatusPaymentPaidMarket;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public StatusPaymentPaidMarket getOldStatusPaymentPaidMarket() {
		return oldStatusPaymentPaidMarket;
	}

	public static StatusPaymentPaidMarket findById(final int id) {
		return Stream.of(StatusPaymentPaidMarket.values()).filter(StatusPaymentPaidMarket.statusPaymentPaidMarketById(id)).findFirst()
				.orElseThrow(UnexpectedStatusOrderException::new);
	}

	private static Predicate<? super StatusPaymentPaidMarket> statusPaymentPaidMarketById(final int id) {
		return statusPaymentPaidMarket -> statusPaymentPaidMarket.getId() == id;
	}

}
