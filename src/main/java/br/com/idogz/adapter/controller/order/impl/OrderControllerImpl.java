package br.com.idogz.adapter.controller.order.impl;

import br.com.idogz.adapter.controller.order.CreateOrderRequestDto;
import br.com.idogz.adapter.controller.order.CreateOrderRequestDto.OrderProductRequestDto;
import br.com.idogz.adapter.controller.order.OrderController;
import br.com.idogz.adapter.controller.order.UpdateOrderStatusDto;
import br.com.idogz.adapter.presenter.order.OrderPresenter;
import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.order.OrderIdNotFoundException;
import br.com.idogz.domain.factory.order.OrderBuilder;
import br.com.idogz.domain.gateway.order.FindOrderByIdDataGateway;
import br.com.idogz.domain.usecase.order.CreateOrderUseCaseInteractor;
import br.com.idogz.domain.usecase.order.ListOrdersUseCaseInteractor;
import br.com.idogz.domain.usecase.order.UpdateOrderStatusUseCaseInteractor;

public class OrderControllerImpl implements OrderController {

	private final CreateOrderUseCaseInteractor createOrder;
	private final ListOrdersUseCaseInteractor listAllOrders;
	private final UpdateOrderStatusUseCaseInteractor updateOrderStatus;
	private final FindOrderByIdDataGateway findOrderByIdDataGateway;
	private final OrderBuilder builder;
	private final OrderPresenter presenter;

	public OrderControllerImpl(final CreateOrderUseCaseInteractor createOrder, final OrderBuilder builder,
			final OrderPresenter presenter, final UpdateOrderStatusUseCaseInteractor updateOrderStatus, final ListOrdersUseCaseInteractor listAllOrders, final FindOrderByIdDataGateway findOrderByIdDataGateway) {
		this.createOrder = createOrder;
		this.listAllOrders = listAllOrders;
		this.findOrderByIdDataGateway = findOrderByIdDataGateway;
		this.builder = builder;
		this.updateOrderStatus = updateOrderStatus;
		this.presenter = presenter;
	}

	@Override
	public Object listAllOrders() throws DomainException {
		final var orders = listAllOrders.execute();
		return presenter.toCollectionModel(orders);
	}

	@Override
	public Object create(final CreateOrderRequestDto orderDto) throws DomainException {
		final var order = toOrder(orderDto);
		final var savedOrder = createOrder.execute(order);
		return presenter.toModel(savedOrder);
	}

	@Override
	public void update(final UpdateOrderStatusDto newOrderStatus) throws DomainException {
		final var id = newOrderStatus.idOrder();
		final var order = findOrderByIdDataGateway.findBy(id).orElseThrow(() -> new OrderIdNotFoundException(id));
		updateOrderStatus.execute(order, newOrderStatus.statusOrder());
	}

	private Order toOrder(final CreateOrderRequestDto request) throws DomainException {
		builder.client(request.clientId());
		builder.date(request.date());
		builder.notes(request.notes());
		addProducts(request);
		return builder.build();
	}

	private void addProducts(final CreateOrderRequestDto entity) throws DomainException {
		for (final OrderProductRequestDto dto : entity.orderProducts()) {
			builder.addProduct(dto.idProduct(), dto.quantity(), null, dto.notes());
		}
	}

}
