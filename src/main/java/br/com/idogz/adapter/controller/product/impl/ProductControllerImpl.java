package br.com.idogz.adapter.controller.product.impl;

import br.com.idogz.adapter.controller.product.CreateProductDto;
import br.com.idogz.adapter.controller.product.ProductController;
import br.com.idogz.adapter.controller.product.UpdateProductDto;
import br.com.idogz.adapter.presenter.product.ProductPresenter;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.factory.product.ProductFactory;
import br.com.idogz.domain.usecase.product.CreateProductUseCaseInteractor;
import br.com.idogz.domain.usecase.product.DeleteProductUseCaseInteractor;
import br.com.idogz.domain.usecase.product.FindProductsByCategoryIdUseCaseInteractor;
import br.com.idogz.domain.usecase.product.UpdateProductUseCaseInteractor;

public class ProductControllerImpl implements ProductController {

	private final FindProductsByCategoryIdUseCaseInteractor findProductsByCategoryId;
	private final CreateProductUseCaseInteractor createProduct;
	private final UpdateProductUseCaseInteractor updateProduct;
	private final DeleteProductUseCaseInteractor deleteProduct;
	private final ProductPresenter presenter;
	private final ProductFactory factory;

	public ProductControllerImpl(final CreateProductUseCaseInteractor createProductUseCase,
			final UpdateProductUseCaseInteractor updateProductUseCase,
			final DeleteProductUseCaseInteractor deleteProductUseCase,
			final ProductPresenter presenter,
			final ProductFactory factory, final FindProductsByCategoryIdUseCaseInteractor findProductsByCategoryId) {
		this.findProductsByCategoryId = findProductsByCategoryId;
		createProduct = createProductUseCase;
		updateProduct = updateProductUseCase;
		deleteProduct = deleteProductUseCase;
		this.presenter = presenter;
		this.factory = factory;
	}

	@Override
	public Object findProductByCategory(final Integer id) throws DomainException {
		final var product = findProductsByCategoryId.execute(id);
		return presenter.toModelList(product);
	}

	@Override
	public Object create(final CreateProductDto dto) throws DomainException {
		final var newProduct = factory.create(dto.idCategory(), dto.name(), dto.price(), dto.description(),
				dto.active(), dto.urlImage());
		final var savedProduct = createProduct.execute(newProduct);
		return presenter.toModel(savedProduct);
	}

	@Override
	public void update(final UpdateProductDto dto) throws DomainException {
		final var newProduct = factory.create(dto.id(), dto.idCategory(), dto.name(), dto.price(), dto.description(),
				dto.active(), dto.urlImage());
		updateProduct.execute(newProduct);
	}

	@Override
	public void remove(final int id) throws DomainException {
		deleteProduct.execute(id);
	}
}
