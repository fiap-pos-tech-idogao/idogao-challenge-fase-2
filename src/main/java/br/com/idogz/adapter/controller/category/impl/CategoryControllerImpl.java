package br.com.idogz.adapter.controller.category.impl;

import br.com.idogz.adapter.controller.category.CategoryController;
import br.com.idogz.adapter.presenter.category.CategoryPresenter;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.usecase.category.FindCategoryByIdUseCaseInteractor;

public class CategoryControllerImpl implements CategoryController {

	private final FindCategoryByIdUseCaseInteractor findCategoryById;
	private final CategoryPresenter presenter;

	public CategoryControllerImpl(final FindCategoryByIdUseCaseInteractor useCase,
			final CategoryPresenter presenter) {
		this.findCategoryById = useCase;
		this.presenter = presenter;
	}

	@Override
	public Object getCategoryWith(final Integer id) throws DomainException {
		final var client = findCategoryById.execute(id);
		return presenter.toModel(client);
	}
}
