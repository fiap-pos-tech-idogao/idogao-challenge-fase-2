package br.com.idogz.adapter.controller.order;

import java.time.LocalDate;
import java.util.List;

public record CreateOrderRequestDto(
		int ticketOrder,
		LocalDate date,
		int clientId,
		String notes,
		List<OrderProductRequestDto> orderProducts
		) {

	public static record OrderProductRequestDto(int idProduct, int quantity, String notes) {
	}

}
