package br.com.idogz.adapter.controller.webhook.impl;

import br.com.idogz.adapter.controller.webhook.WebhookController;
import br.com.idogz.adapter.controller.webhook.WebhookRegisterPaymentDto;
import br.com.idogz.domain.entity.StatusPayment;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.order.OrderIdPaymentNotFoundException;
import br.com.idogz.domain.gateway.order.FindOrderByIdPaymentDataGateway;
import br.com.idogz.domain.usecase.payment.RegisterPaymentUseCaseInteractor;

public class IdogzPagoWebhookControllerImpl implements WebhookController {

	private final RegisterPaymentUseCaseInteractor registerPaymentUseCaseInteractor;
	private final FindOrderByIdPaymentDataGateway findOrderByIdPaymentDataGateway;

	public IdogzPagoWebhookControllerImpl(final RegisterPaymentUseCaseInteractor registerPaymentUseCaseInteractor,
			final FindOrderByIdPaymentDataGateway findOrderByIdPaymentDataGateway) {
		this.registerPaymentUseCaseInteractor = registerPaymentUseCaseInteractor;
		this.findOrderByIdPaymentDataGateway = findOrderByIdPaymentDataGateway;
	}

	@Override
	public void registerPayment(final WebhookRegisterPaymentDto whRegisterPaymentDto) throws DomainException {
		final var idPayment = whRegisterPaymentDto.data().id();
		final var action = whRegisterPaymentDto.action();
		final var statusPayment = StatusPayment.findByDescription(action);
		final var order = findOrderByIdPaymentDataGateway.findByIdPayment(idPayment)
				.orElseThrow(() -> new OrderIdPaymentNotFoundException(idPayment));
		registerPaymentUseCaseInteractor.execute(order, statusPayment);
	}

}
