package br.com.idogz.adapter.controller.webhook.impl;

import java.util.Objects;

import br.com.idogz.ApiMercadoPagoPropertiesFactory;
import br.com.idogz.adapter.controller.payment.StatusPaymentPaidMarket;
import br.com.idogz.adapter.controller.webhook.WebhookController;
import br.com.idogz.adapter.controller.webhook.WebhookRegisterPaymentDto;
import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.entity.StatusPayment;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.order.OrderIdPaymentNotFoundException;
import br.com.idogz.domain.exception.payment.InvalidUserMercadoPagoException;
import br.com.idogz.domain.gateway.order.FindOrderByIdPaymentDataGateway;
import br.com.idogz.domain.gateway.payment.FindPaymentOrderDataGateway;
import br.com.idogz.domain.usecase.payment.RegisterPaymentUseCaseInteractor;

public class MercadoPagoWebhookControllerImpl implements WebhookController {

	private ApiMercadoPagoPropertiesFactory propertiesMercadoPago;
	private final RegisterPaymentUseCaseInteractor registerPaymentUseCaseInteractor;
	private final FindPaymentOrderDataGateway findPaymentOrderDataGateway;
	private final FindOrderByIdPaymentDataGateway findOrderByIdPaymentDataGateway;

	public MercadoPagoWebhookControllerImpl(final RegisterPaymentUseCaseInteractor registerPaymentUseCaseInteractor,
			final FindPaymentOrderDataGateway findPaymentOrderDataGateway,
			final FindOrderByIdPaymentDataGateway findOrderByIdPaymentDataGateway,
			final ApiMercadoPagoPropertiesFactory propertiesMercadoPago) {
		this.registerPaymentUseCaseInteractor = registerPaymentUseCaseInteractor;
		this.findPaymentOrderDataGateway = findPaymentOrderDataGateway;
		this.findOrderByIdPaymentDataGateway = findOrderByIdPaymentDataGateway;
	}

	@Override
	public void registerPayment(final WebhookRegisterPaymentDto whRegisterPaymentDto) throws DomainException {
		final var idPayment = whRegisterPaymentDto.data().id();
		final var payment = findPaymentOrderDataGateway.findPayment(idPayment.toString());


		var statusPayment = StatusPayment.PENDING;
		if (payment.status().equalsIgnoreCase(StatusPaymentPaidMarket.APPROVED.getDescription())) {
			statusPayment = StatusPayment.CONFIRMED;
		}
		if (payment.status().equalsIgnoreCase(StatusPaymentPaidMarket.CANCELLED.getDescription())) {
			statusPayment = StatusPayment.REFUSED;
		}

		final var order = findOrderByIdPaymentDataGateway.findByIdPayment(idPayment)
				.orElseThrow(() -> new OrderIdPaymentNotFoundException(idPayment));

		validate(whRegisterPaymentDto, order);

		registerPaymentUseCaseInteractor.execute(order, statusPayment);
	}

	private void validate(final WebhookRegisterPaymentDto registerPaymentDto, final Order order)
			throws InvalidUserMercadoPagoException {

		if (Objects.isNull(registerPaymentDto) || Objects.isNull(registerPaymentDto.user_id())) {
			throw new InvalidUserMercadoPagoException("");
		}

		// Validate user
		if (!registerPaymentDto.user_id().equalsIgnoreCase(propertiesMercadoPago.userId()) || registerPaymentDto.type() == null
				|| !registerPaymentDto.type().equalsIgnoreCase(propertiesMercadoPago.type())) {
			final var invalidUserMercadoPagoException = new InvalidUserMercadoPagoException(registerPaymentDto.user_id());
			System.out.println("WEBHOOK REGISTRATION ERROR ====>>>> ["
					+ invalidUserMercadoPagoException + "]");
			throw invalidUserMercadoPagoException;
		}

		// Only records payment if it is pending
		if (!StatusPayment.PENDING.equals(order.statusPayment())) {
			final var invalidUserMercadoPagoException = new InvalidUserMercadoPagoException(registerPaymentDto.user_id());
			System.out.println("WEBHOOK REGISTRATION VALIDATION ERROR ====>>>> ["
					+ invalidUserMercadoPagoException + "]");
			throw invalidUserMercadoPagoException;
		}
	}

}
