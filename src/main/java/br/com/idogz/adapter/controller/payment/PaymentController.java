package br.com.idogz.adapter.controller.payment;

import java.util.UUID;

import br.com.idogz.domain.exception.DomainException;

public interface PaymentController {

	Object verifyStatusPayment(UUID idOrder) throws DomainException;

}
