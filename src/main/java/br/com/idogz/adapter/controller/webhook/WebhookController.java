package br.com.idogz.adapter.controller.webhook;

import br.com.idogz.domain.exception.DomainException;

public interface WebhookController {

	void registerPayment(WebhookRegisterPaymentDto whRegisterPaymentDto) throws DomainException;

}
