package br.com.idogz.adapter.controller.webhook;

public record WebhookRegisterPaymentDto(
		String action,
		String type,
		String user_id,
		WebhookRegisterPaymentDataDto data) {
	public record WebhookRegisterPaymentDataDto(Integer id) {
	}
}
