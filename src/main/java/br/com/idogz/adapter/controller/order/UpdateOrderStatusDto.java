package br.com.idogz.adapter.controller.order;

import java.util.UUID;

public record UpdateOrderStatusDto(UUID idOrder, int statusOrder) {

}
