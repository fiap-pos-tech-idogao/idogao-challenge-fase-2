package br.com.idogz.adapter.controller.payment.impl;

import java.util.UUID;

import br.com.idogz.adapter.controller.payment.PaymentController;
import br.com.idogz.adapter.presenter.payment.PaymentPresenter;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.usecase.payment.VerifyStatusPaymentOrderUserCaseInteractor;

public class PaymentControllerImpl implements PaymentController {

	private final VerifyStatusPaymentOrderUserCaseInteractor verifyStatusPaymentOrderUserCase;
	private final PaymentPresenter paymentPresenter;

	public PaymentControllerImpl(final VerifyStatusPaymentOrderUserCaseInteractor verifyStatusPaymentOrderUserCase,
			final PaymentPresenter paymentPresenter) {
		this.verifyStatusPaymentOrderUserCase = verifyStatusPaymentOrderUserCase;
		this.paymentPresenter = paymentPresenter;
	}

	@Override
	public Object verifyStatusPayment(final UUID idOrder) throws DomainException {
		final var order = verifyStatusPaymentOrderUserCase.execute(idOrder);
		return paymentPresenter.toModel(order);
	}

}
