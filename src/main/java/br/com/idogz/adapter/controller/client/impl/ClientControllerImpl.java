package br.com.idogz.adapter.controller.client.impl;

import br.com.idogz.adapter.controller.client.ClientController;
import br.com.idogz.adapter.controller.client.CreateClientRequestDto;
import br.com.idogz.adapter.presenter.client.ClientPresenter;
import br.com.idogz.domain.entity.Cpf;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.factory.client.ClientFactory;
import br.com.idogz.domain.usecase.client.CreateClientUseCaseInteractor;
import br.com.idogz.domain.usecase.client.FindClientByCpfUseCaseInteractor;

public class ClientControllerImpl implements ClientController {

	private final FindClientByCpfUseCaseInteractor findClient;
	private final CreateClientUseCaseInteractor createClient;
	private final ClientPresenter presenter;
	private final ClientFactory factory;

	public ClientControllerImpl(final FindClientByCpfUseCaseInteractor findClient,
			final ClientPresenter presenter, final CreateClientUseCaseInteractor createClient, final ClientFactory factory) {
		this.findClient = findClient;
		this.createClient = createClient;
		this.presenter = presenter;
		this.factory = factory;
	}

	@Override
	public Object findClientWith(final String cpfValue) throws DomainException {
		final var cpf = new Cpf(cpfValue);
		final var client = findClient.execute(cpf);
		return presenter.toModel(client);
	}

	@Override
	public Object create(final CreateClientRequestDto clientDto) throws DomainException {
		final var newClient = factory.create(clientDto.name(), clientDto.cpf(), clientDto.email());
		final var savedClient = createClient.execute(newClient);
		return presenter.toModel(savedClient);
	}

}
