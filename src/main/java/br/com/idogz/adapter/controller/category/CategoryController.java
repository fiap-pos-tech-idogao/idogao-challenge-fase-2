package br.com.idogz.adapter.controller.category;

import br.com.idogz.domain.exception.DomainException;

public interface CategoryController {

	Object getCategoryWith(Integer id) throws DomainException;
}
