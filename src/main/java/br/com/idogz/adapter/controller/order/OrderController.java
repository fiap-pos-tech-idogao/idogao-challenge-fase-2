package br.com.idogz.adapter.controller.order;

import br.com.idogz.domain.exception.DomainException;

public interface OrderController {

	/**
	 * Updates order status
	 *
	 * @param newOrderStatus
	 * @throws DomainException
	 */
	void update(final UpdateOrderStatusDto newOrderStatus) throws DomainException;

	Object create(final CreateOrderRequestDto orderDto) throws DomainException;

	Object listAllOrders() throws DomainException;

}
