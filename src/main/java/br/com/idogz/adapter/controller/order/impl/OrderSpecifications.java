package br.com.idogz.adapter.controller.order.impl;

import org.springframework.data.jpa.domain.Specification;

import br.com.idogz.domain.entity.StatusOrder;
import br.com.idogz.external.repository.order.entity.OrderEntity;
import br.com.idogz.external.repository.order.entity.StatusOrderEntity;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Path;

public class OrderSpecifications {

	private static final String STATUS = "status";

	public static Specification<OrderEntity> isNot(final StatusOrder status) {
		return (root, query, criteriaBuilder) -> {
			root.fetch("client", JoinType.INNER);
			root.fetch(OrderSpecifications.STATUS, JoinType.INNER);
			root.fetch("orderProducts", JoinType.INNER);
			final Path<StatusOrderEntity> path = root.get(OrderSpecifications.STATUS);
			return criteriaBuilder.notEqual(path.get("idStatus"), status.getId());
		};
	}

}
