package br.com.idogz.adapter.repository.product;

public record CreateProductDataDto(Integer id, Integer categoryId, String name, Double price, String description,
		Boolean active, String urlImage) {

}
