package br.com.idogz.adapter.repository.product;

import br.com.idogz.adapter.repository.category.PersistedCategoryDto;

public record ProductDto(Integer id, PersistedCategoryDto categoryDto, String name, Double price, String description,
		Boolean active, String urlImage) {

}
