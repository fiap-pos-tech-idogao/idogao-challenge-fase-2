package br.com.idogz.adapter.repository.order;

import java.time.LocalDate;
import java.util.UUID;

public record CreateOrderDataDto(UUID id, LocalDate date, Double totalValue, Integer idStatus, Integer idStatusPayment,
		Integer idClient, String notes, Integer idPayment, String urlQrcode) {

}
