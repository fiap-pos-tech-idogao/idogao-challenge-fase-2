package br.com.idogz.adapter.repository.client;

public record CreateClientDataDto(String name, String cpf, String email) {

}
