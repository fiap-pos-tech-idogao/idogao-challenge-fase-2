package br.com.idogz.adapter.repository.category;

import java.util.Optional;

public interface CategoryRepository {

	Optional<PersistedCategoryDto> findById(Integer idCategory);

}
