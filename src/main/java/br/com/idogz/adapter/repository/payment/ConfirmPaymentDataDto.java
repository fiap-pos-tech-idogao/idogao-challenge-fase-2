package br.com.idogz.adapter.repository.payment;

public record ConfirmPaymentDataDto(Integer id, ConfirmPaymentPointOfInteractionDataDto point_of_interaction) {

    public record ConfirmPaymentPointOfInteractionDataDto(ConfirmPaymentPointOfTransactionDataDto transaction_data) {

        public record ConfirmPaymentPointOfTransactionDataDto(String ticket_url) {

        }
    
    }
    
}
