package br.com.idogz.adapter.repository.order;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import br.com.idogz.adapter.gateway.order.impl.UpdateStatusOrderDataDto;
import br.com.idogz.adapter.gateway.order.impl.UpdateStatusPaymentOrderDataDto;

public interface OrderRepository {

	OrderIDTicketDataView save(CreateOrderDataDto dto);

	void updateStatus(UpdateStatusOrderDataDto dto);

	void updateStatusPayment(UpdateStatusPaymentOrderDataDto dto);

	List<OrderDataDto> findAll();

	Optional<OrderDataDto> findBy(UUID id);

	Optional<OrderDataDto> findByIdPayment(Integer id);

}
