package br.com.idogz.adapter.repository.client;

import java.util.Optional;

import br.com.idogz.domain.exception.DomainException;

public interface ClientRepository {

	ClientDataDto save(CreateClientDataDto dto) throws DomainException;

	Optional<ClientDataDto> getClientByCpf(String cpf);

	Optional<ClientDataDto> findById(int id);

}
