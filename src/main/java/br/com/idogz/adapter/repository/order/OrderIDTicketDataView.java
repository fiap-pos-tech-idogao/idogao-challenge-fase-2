package br.com.idogz.adapter.repository.order;

public interface OrderIDTicketDataView {

	Integer getTicketOrder();

}
