package br.com.idogz.adapter.repository.payment;

public record CreatePayerDataDto(String email) {

}
