package br.com.idogz.adapter.repository.product;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

public interface ProductRepository {

	Optional<ProductDataDto> getProductByName(@Param("_name") String name);

	Optional<ProductDataDto> getProductById(@Param("_productid") Integer productId);

	List<ProductDataDto> getProductByCategoryId(@Param("_categoryid") Integer categoryId);

	ProductDataDto create(CreateProductDataDto dto);

	void update(ProductDataDto dto);

	void delete(CreateProductDataDto dto);

}
