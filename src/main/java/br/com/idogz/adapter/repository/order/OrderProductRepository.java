package br.com.idogz.adapter.repository.order;

import br.com.idogz.adapter.gateway.order.impl.OrderProductDataDto;

public interface OrderProductRepository {

	void saveOrderProduct(OrderProductDataDto orderProductDataDto);

}
