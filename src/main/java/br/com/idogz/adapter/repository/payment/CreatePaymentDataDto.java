package br.com.idogz.adapter.repository.payment;

import java.math.BigDecimal;

public record CreatePaymentDataDto(String payment_method_id, Integer installments, BigDecimal transaction_amount,
        String notification_url,
        CreatePayerDataDto payer) {
}
