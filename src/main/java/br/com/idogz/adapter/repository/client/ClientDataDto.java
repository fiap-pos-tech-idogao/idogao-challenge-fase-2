package br.com.idogz.adapter.repository.client;

public record ClientDataDto(Integer id, String name, String cpf, String email) {

}
