package br.com.idogz.adapter.repository.category;

public record PersistedCategoryDto(Integer id, String name) {

}
