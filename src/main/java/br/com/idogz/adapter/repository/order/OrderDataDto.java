package br.com.idogz.adapter.repository.order;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import br.com.idogz.adapter.repository.client.ClientDataDto;
import br.com.idogz.adapter.repository.product.ProductDto;

public record OrderDataDto(UUID id, LocalDate date, Integer ticketOrder, Integer statusOrder, Integer statusPayment, Integer idPayment,
		ClientDataDto clientDto,
		String notes, List<OrderProductDataDto> orderProductDtos) {

	public static record OrderProductDataDto(ProductDto productDto, Integer quantity, String notes) {
	}

}
