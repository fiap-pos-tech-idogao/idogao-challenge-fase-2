package br.com.idogz.adapter.presenter.payment;

import br.com.idogz.domain.entity.Order;

public interface PaymentPresenter {

	Object toModel(Order statusPayment);

}
