package br.com.idogz.adapter.presenter.product;

import java.util.List;

import br.com.idogz.domain.entity.Product;

public interface ProductPresenter {

	Object toModel(Product product);

	Object toModelList(List<Product> product);
}
