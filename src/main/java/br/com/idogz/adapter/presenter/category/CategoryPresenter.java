package br.com.idogz.adapter.presenter.category;

import br.com.idogz.domain.entity.Category;

public interface CategoryPresenter {

	Object toModel(Category category);

}
