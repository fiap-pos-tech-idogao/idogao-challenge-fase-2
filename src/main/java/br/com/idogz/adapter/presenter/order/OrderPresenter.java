package br.com.idogz.adapter.presenter.order;

import br.com.idogz.domain.entity.Order;

public interface OrderPresenter {

	Object toModel(final Order order);

	Object toCollectionModel(final Iterable<? extends Order> entities);

}
