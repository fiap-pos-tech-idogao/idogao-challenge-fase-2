package br.com.idogz.adapter.presenter.client;

import br.com.idogz.domain.entity.Client;

public interface ClientPresenter {

	Object toModel(Client client);

}
