package br.com.idogz.adapter.service;

import br.com.idogz.adapter.repository.payment.ConfirmPaymentDataDto;
import br.com.idogz.adapter.repository.payment.CreatePaymentDataDto;
import br.com.idogz.domain.entity.Order;

public interface SendPaymentOrderService {

	ConfirmPaymentDataDto sendPayment(CreatePaymentDataDto dto, Order order);

}
