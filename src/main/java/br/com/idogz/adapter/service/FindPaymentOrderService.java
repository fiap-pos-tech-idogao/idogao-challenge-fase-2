package br.com.idogz.adapter.service;

import br.com.idogz.adapter.repository.payment.FindPaymentDataDto;

public interface FindPaymentOrderService {

	FindPaymentDataDto findPayment(String idPayment);

}
