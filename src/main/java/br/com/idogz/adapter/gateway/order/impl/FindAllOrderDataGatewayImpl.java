package br.com.idogz.adapter.gateway.order.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.idogz.adapter.repository.category.PersistedCategoryDto;
import br.com.idogz.adapter.repository.order.OrderDataDto;
import br.com.idogz.adapter.repository.order.OrderDataDto.OrderProductDataDto;
import br.com.idogz.adapter.repository.order.OrderRepository;
import br.com.idogz.adapter.repository.product.ProductDto;
import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.factory.order.OrderBuilder;
import br.com.idogz.domain.gateway.order.FindAllOrdersDataGateway;

public class FindAllOrderDataGatewayImpl implements FindAllOrdersDataGateway {

	private final OrderRepository repository;
	private final OrderBuilder builder;

	public FindAllOrderDataGatewayImpl(final OrderRepository repository, final OrderBuilder builder) {
		this.repository = repository;
		this.builder = builder;
	}

	@Override
	public List<Order> listAllOrders() throws DomainException {
		final var orders = new ArrayList<Order>();
		final var orderEntities = repository.findAll();
		for (final OrderDataDto dto : orderEntities) {
			final var order = toOrder(dto);
			orders.add(order);
		}
		return orders;
	}

	private Order toOrder(final OrderDataDto dto) {
		try {
			builder.idOrder(dto.id());
			builder.client(dto.clientDto().id());
			builder.ticketOrder(dto.ticketOrder());
			builder.date(dto.date());
			builder.notes(dto.notes());
			builder.statusOrder(dto.statusOrder());
			builder.statusPayment(dto.statusPayment());
			addProducts(dto);
			return builder.build();
		} catch (final DomainException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void addProducts(final OrderDataDto dto) throws DomainException {
		for (final OrderProductDataDto orderProductEntity : dto.orderProductDtos()) {
			final int productId = getProductId(orderProductEntity);
			final var productName = getProductName(orderProductEntity);
			final int categoryId = getProductCategoryId(orderProductEntity);
			final var categoryName = getProductCategoryName(orderProductEntity);
			final var price = getProductPrice(orderProductEntity);
			final var description = getProductDescription(orderProductEntity);
			final var active = getProductActive(orderProductEntity);
			final var urlImage = getProductUrlImage(orderProductEntity);
			final int quantity = getProductOrderQuantity(orderProductEntity);
			final var notes = getProductOrderNotes(orderProductEntity);
			builder.addProduct(categoryId, categoryName, productName, price, description, active, urlImage, quantity, notes, productId);
		}
	}

	private PersistedCategoryDto getProductCategory(final ProductDto product) {
		return product.categoryDto();
	}

	private ProductDto getProduct(final OrderProductDataDto orderProductEntity) {
		return orderProductEntity.productDto();
	}

	private Integer getProductId(final OrderProductDataDto orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.id();
	}

	private String getProductName(final OrderProductDataDto orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.name();
	}

	private String getProductCategoryName(final OrderProductDataDto orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		final var categoryEntity = getProductCategory(product);
		return categoryEntity.name();
	}

	private Integer getProductCategoryId(final OrderProductDataDto orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		final var categoryEntity = getProductCategory(product);
		return categoryEntity.id();
	}

	private Double getProductPrice(final OrderProductDataDto orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.price();
	}

	private String getProductDescription(final OrderProductDataDto orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.description();
	}

	private Boolean getProductActive(final OrderProductDataDto orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.active();
	}

	private String getProductUrlImage(final OrderProductDataDto orderProductEntity) {
		final var product = getProduct(orderProductEntity);
		return product.urlImage();
	}

	private Integer getProductOrderQuantity(final OrderProductDataDto orderProductEntity) {
		return orderProductEntity.quantity();
	}

	private String getProductOrderNotes(final OrderProductDataDto orderProductEntity) {
		return orderProductEntity.notes();
	}
}
