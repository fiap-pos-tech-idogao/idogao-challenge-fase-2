package br.com.idogz.adapter.gateway.product.impl;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.idogz.adapter.repository.product.ProductDataDto;
import br.com.idogz.adapter.repository.product.ProductRepository;
import br.com.idogz.domain.entity.Name;
import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.DuplicatedNameException;
import br.com.idogz.domain.gateway.product.UpdateProductDataGateway;

public class UpdateProductGatewayImpl implements UpdateProductDataGateway {

	private static final String ERROR_DUPLICATE_KEY_VALUE_VIOLATES_UNIQUE_CONSTRAINT = "ERROR: duplicate key value violates unique constraint";

	private final ProductRepository repository;
	private ProductDataDto dto;

	public UpdateProductGatewayImpl(final ProductRepository repository) {
		this.repository = repository;
	}

	@Override
	public void update(final Product product) throws DomainException {
		try {
			dto = new ProductDataDto(product.id(), product.category().id(), product.name().toString(), product.price(), product.description(), product.active(), product.urlImage());
			repository.update(dto);
		} catch (final DataIntegrityViolationException e) {
			throw UpdateProductGatewayImpl.handlerException(dto, e);
		}
	}

	private static DomainException handlerException(final ProductDataDto dto, final DataIntegrityViolationException e)
			throws DomainException {
		final var message = e.getMostSpecificCause().getMessage();
		if (message.contains(UpdateProductGatewayImpl.ERROR_DUPLICATE_KEY_VALUE_VIOLATES_UNIQUE_CONSTRAINT)) {
			return new DuplicatedNameException(new Name(dto.name()));
		}
		return new DomainException(message);
	}
}
