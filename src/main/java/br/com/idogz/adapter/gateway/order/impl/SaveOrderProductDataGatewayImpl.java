package br.com.idogz.adapter.gateway.order.impl;

import java.util.List;

import br.com.idogz.adapter.repository.order.OrderProductRepository;
import br.com.idogz.domain.entity.OrderProduct;
import br.com.idogz.domain.gateway.order.SaveOrderProductDataGateway;

public class SaveOrderProductDataGatewayImpl implements SaveOrderProductDataGateway {

	private final OrderProductRepository repository;

	public SaveOrderProductDataGatewayImpl(final OrderProductRepository repository) {
		this.repository = repository;
	}

	@Override
	public void saveAll(final List<OrderProduct> orderProducts) {
		orderProducts.forEach(this::add);
	}

	private void add(final OrderProduct orderProduct) {
		final var orderProductDataDto = new OrderProductDataDto(orderProduct.idOrder(),
				orderProduct.idProduct(),
				orderProduct.quantity(), orderProduct.notes());
		repository.saveOrderProduct(orderProductDataDto);
	}

}
