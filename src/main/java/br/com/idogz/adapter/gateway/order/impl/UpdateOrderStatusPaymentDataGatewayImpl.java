package br.com.idogz.adapter.gateway.order.impl;

import br.com.idogz.adapter.repository.order.OrderRepository;
import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.order.OrderIdNotFoundException;
import br.com.idogz.domain.gateway.order.UpdateOrderStatusPaymentDataGateway;

public class UpdateOrderStatusPaymentDataGatewayImpl implements UpdateOrderStatusPaymentDataGateway {

	private final OrderRepository repository;

	public UpdateOrderStatusPaymentDataGatewayImpl(final OrderRepository repository) {
		this.repository = repository;
	}

	@Override
	public Order updateStatusPayment(final Order order) throws OrderIdNotFoundException {
		final var dto = new UpdateStatusPaymentOrderDataDto(order.id(), order.status().getId(), order.statusPayment().getId());
		repository.updateStatusPayment(dto);
		return order;
	}

}
