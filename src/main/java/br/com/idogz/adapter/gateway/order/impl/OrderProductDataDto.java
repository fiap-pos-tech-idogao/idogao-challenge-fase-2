package br.com.idogz.adapter.gateway.order.impl;

import java.util.UUID;

public record OrderProductDataDto(UUID idOrder, Integer idProduct, Integer quantity, String notes) {

}
