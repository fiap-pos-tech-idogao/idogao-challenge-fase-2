package br.com.idogz.adapter.gateway.product.impl;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.idogz.adapter.repository.product.CreateProductDataDto;
import br.com.idogz.adapter.repository.product.ProductDataDto;
import br.com.idogz.adapter.repository.product.ProductRepository;
import br.com.idogz.domain.entity.Name;
import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.DuplicatedNameException;
import br.com.idogz.domain.factory.product.ProductFactory;
import br.com.idogz.domain.gateway.product.CreateProductDataGateway;

public class CreateProductGatewayImpl implements CreateProductDataGateway {

	private static final String ERROR_DUPLICATE_KEY_VALUE_VIOLATES_UNIQUE_CONSTRAINT = "ERROR: duplicate key value violates unique constraint";

	private final ProductRepository repository;
	private final ProductFactory factory;
	private CreateProductDataDto dto;
	private ProductDataDto createdProductDataDto;

	public CreateProductGatewayImpl(final ProductRepository repository, final ProductFactory factory) {
		this.factory = factory;
		this.repository = repository;
	}

	@Override
	public Product save(final Product product) throws DomainException {
		dto = new CreateProductDataDto(product.id(), product.category().id(), product.name().toString(),
				product.price(), product.description(), product.active(), product.urlImage());
		save();
		return toProduct();
	}

	private void save() throws DomainException {
		try {
			createdProductDataDto = repository.create(dto);
		} catch (final DataIntegrityViolationException e) {
			throw CreateProductGatewayImpl.handlerException(dto, e);
		}
	}

	private static DomainException handlerException(final CreateProductDataDto dto,
			final DataIntegrityViolationException e)
					throws DomainException {
		final var message = e.getMostSpecificCause().getMessage();
		if (message.contains(CreateProductGatewayImpl.ERROR_DUPLICATE_KEY_VALUE_VIOLATES_UNIQUE_CONSTRAINT)) {
			return new DuplicatedNameException(new Name(dto.name()));
		}
		return new DomainException(message);
	}

	private Product toProduct() throws DomainException {
		return factory.create(createdProductDataDto.id(), createdProductDataDto.categoryId(),
				createdProductDataDto.name(), createdProductDataDto.price(), createdProductDataDto.description(),
				createdProductDataDto.active(), createdProductDataDto.urlImage());
	}

}
