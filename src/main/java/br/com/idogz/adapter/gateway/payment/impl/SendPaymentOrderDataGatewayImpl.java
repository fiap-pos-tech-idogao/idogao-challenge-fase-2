package br.com.idogz.adapter.gateway.payment.impl;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.idogz.ApiMercadoPagoPropertiesFactory;
import br.com.idogz.adapter.repository.payment.ConfirmPaymentDataDto;
import br.com.idogz.adapter.repository.payment.CreatePayerDataDto;
import br.com.idogz.adapter.repository.payment.CreatePaymentDataDto;
import br.com.idogz.adapter.service.SendPaymentOrderService;
import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.payment.SendPaymentOrderDataGateway;

public class SendPaymentOrderDataGatewayImpl implements SendPaymentOrderDataGateway {

	@Autowired
	private ApiMercadoPagoPropertiesFactory propertiesMercadoPago;

	private final SendPaymentOrderService service;

	public SendPaymentOrderDataGatewayImpl(final SendPaymentOrderService service) {
		this.service = service;
	}

	@Override
	public ConfirmPaymentDataDto sendPayment(final Order order) throws DomainException {
		return execute(order);
	}

	private ConfirmPaymentDataDto execute(final Order order) {
		final var newPaymentData = new CreatePaymentDataDto(propertiesMercadoPago.paymentMethodId(),
				propertiesMercadoPago.installments(), order.totalValue(), propertiesMercadoPago.urlWebhookPayment(),
				new CreatePayerDataDto(order.client().emailAddress()));
		return service.sendPayment(newPaymentData, order);
	}
}
