package br.com.idogz.adapter.gateway.order.impl;

import java.util.UUID;

public record UpdateStatusPaymentOrderDataDto(UUID id, int status, int statusPayment) {

}
