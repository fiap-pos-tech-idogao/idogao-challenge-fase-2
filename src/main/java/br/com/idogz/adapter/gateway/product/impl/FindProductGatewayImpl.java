package br.com.idogz.adapter.gateway.product.impl;

import java.util.List;
import java.util.Optional;

import br.com.idogz.adapter.repository.product.ProductDataDto;
import br.com.idogz.adapter.repository.product.ProductRepository;
import br.com.idogz.domain.entity.Name;
import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.factory.product.ProductFactory;
import br.com.idogz.domain.gateway.product.FindProductDataGateway;

public class FindProductGatewayImpl implements FindProductDataGateway {

	private final ProductRepository repository;
	private final ProductFactory factory;

	public FindProductGatewayImpl(final ProductRepository repository, final ProductFactory factory) {
		this.repository = repository;
		this.factory = factory;
	}

	@Override
	public Optional<Product> findByName(final Name name) {
		return repository.getProductByName(name.toString()).map(this::toProduct);
	}

	@Override
	public Optional<Product> findBy(final int id) {
		return repository.getProductById(id).map(this::toProduct);
	}

	@Override
	public List<Product> findByCategory(final int id) {
		return repository.getProductByCategoryId(id).stream().map(this::toProduct).toList();
	}

	private Product toProduct(final ProductDataDto dto) {
		try {
			return factory.create(dto.id(), dto.categoryId(), dto.name(), dto.price(), dto.description(),
					dto.active(), dto.urlImage());
		} catch (final DomainException e) {
			e.printStackTrace();
			return null;
		}
	}
}
