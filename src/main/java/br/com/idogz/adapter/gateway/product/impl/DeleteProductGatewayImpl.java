package br.com.idogz.adapter.gateway.product.impl;

import br.com.idogz.adapter.repository.product.CreateProductDataDto;
import br.com.idogz.adapter.repository.product.ProductRepository;
import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.product.DeleteProductDataGateway;

public class DeleteProductGatewayImpl implements DeleteProductDataGateway {

	private final ProductRepository repository;

	public DeleteProductGatewayImpl(final ProductRepository repository) {
		this.repository = repository;
	}

	@Override
	public void delete(final Product product) throws DomainException {
		final var dto = new CreateProductDataDto(product.id(), product.category().id(),
				product.name().toString(), product.price(), product.description(), false,
				product.urlImage());
		repository.delete(dto);
	}
}
