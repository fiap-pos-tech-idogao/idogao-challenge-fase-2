package br.com.idogz.adapter.gateway.order.impl;

import java.util.UUID;

public record UpdateStatusOrderDataDto(UUID id, int status) {

}
