package br.com.idogz.adapter.gateway.client.impl;

import br.com.idogz.adapter.repository.client.ClientDataDto;
import br.com.idogz.adapter.repository.client.ClientRepository;
import br.com.idogz.adapter.repository.client.CreateClientDataDto;
import br.com.idogz.domain.entity.Client;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.factory.client.ClientFactory;
import br.com.idogz.domain.gateway.client.SaveClientDataGateway;

public class SaveClientGatewayImpl implements SaveClientDataGateway {

	private final ClientRepository repository;
	private final ClientFactory factory;
	private CreateClientDataDto newClientDataDto;
	private ClientDataDto savedClientDto;

	public SaveClientGatewayImpl(final ClientRepository repository, final ClientFactory factory) {
		this.repository = repository;
		this.factory = factory;
	}

	@Override
	public Client save(final Client client) throws DomainException {
		newClientDataDto = new CreateClientDataDto(client.name(), client.unformattingCpf(), client.emailAddress());
		save();
		return toClient();
	}

	private void save() throws DomainException {
		savedClientDto = repository.save(newClientDataDto);
	}

	private Client toClient() throws DomainException {
		return factory.create(savedClientDto.id(), savedClientDto.name(), savedClientDto.cpf(), savedClientDto.email());
	}

}
