package br.com.idogz.adapter.gateway.order.impl;

import br.com.idogz.adapter.repository.order.CreateOrderDataDto;
import br.com.idogz.adapter.repository.order.OrderIDTicketDataView;
import br.com.idogz.adapter.repository.order.OrderRepository;
import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.order.CreateOrderDataGateway;

public class SaveOrderDataGatewayImpl implements CreateOrderDataGateway {

	private final OrderRepository repository;

	public SaveOrderDataGatewayImpl(final OrderRepository repository) {
		this.repository = repository;
	}

	@Override
	public Order create(final Order order) throws DomainException {
		final var savedOrderEntity = execute(order);
		order.updateTicket(savedOrderEntity.getTicketOrder());
		return order;
	}

	private OrderIDTicketDataView execute(final Order order) {
		final var newOrderData = new CreateOrderDataDto(order.id(), order.date(), order.totalValue().doubleValue(),
				order.status().getId(), order.statusPayment().getId(), order.client().id(), order.notes().getValue(),  order.idPayment(), order.urlQrcode());
		return repository.save(newOrderData);
	}

}
