package br.com.idogz.adapter.gateway.category.impl;

import java.util.Optional;

import br.com.idogz.adapter.repository.category.PersistedCategoryDto;
import br.com.idogz.adapter.repository.category.CategoryRepository;
import br.com.idogz.domain.entity.Category;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.factory.category.CategoryFactory;
import br.com.idogz.domain.gateway.category.FindCategoryDataGateway;

public class FindCategoryGatewayImpl implements FindCategoryDataGateway {

	private final CategoryRepository repository;
	private final CategoryFactory factory;

	public FindCategoryGatewayImpl(final CategoryRepository repository, final CategoryFactory factory) {
		this.repository = repository;
		this.factory = factory;
	}

	@Override
	public Optional<Category> findBy(final Integer idCategory) {
		return repository.findById(idCategory).map(this::toClient);
	}

	private Category toClient(final PersistedCategoryDto dto) {
		try {
			return factory.create(dto.id(), dto.name());
		} catch (final DomainException e) {
			e.printStackTrace();
			return null;
		}
	}
}
