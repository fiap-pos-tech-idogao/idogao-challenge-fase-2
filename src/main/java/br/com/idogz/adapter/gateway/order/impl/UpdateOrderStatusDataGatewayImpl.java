package br.com.idogz.adapter.gateway.order.impl;

import br.com.idogz.adapter.repository.order.OrderRepository;
import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.order.OrderIdNotFoundException;
import br.com.idogz.domain.gateway.order.UpdateOrderStatusDataGateway;

public class UpdateOrderStatusDataGatewayImpl implements UpdateOrderStatusDataGateway {

	private final OrderRepository repository;

	public UpdateOrderStatusDataGatewayImpl(final OrderRepository repository) {
		this.repository = repository;
	}

	@Override
	public Order updateStatus(final Order order) throws OrderIdNotFoundException {
		final var dto = new UpdateStatusOrderDataDto(order.id(), order.status().getId());
		repository.updateStatus(dto);
		return order;
	}

}
