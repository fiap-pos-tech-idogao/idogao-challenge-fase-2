package br.com.idogz.adapter.gateway.client.impl;

import java.util.Optional;

import br.com.idogz.adapter.repository.client.ClientDataDto;
import br.com.idogz.adapter.repository.client.ClientRepository;
import br.com.idogz.domain.entity.Client;
import br.com.idogz.domain.entity.Cpf;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.factory.client.ClientFactory;
import br.com.idogz.domain.gateway.client.FindClientDataGateway;

public class FindClientGatewayImpl implements FindClientDataGateway {

	private final ClientRepository repository;
	private final ClientFactory factory;

	public FindClientGatewayImpl(final ClientRepository repository, final ClientFactory factory) {
		this.repository = repository;
		this.factory = factory;
	}

	@Override
	public Optional<Client> find(final Cpf cpf) {
		return repository.getClientByCpf(cpf.unformattingValue()).map(this::toClient);
	}

	@Override
	public Optional<Client> find(final int id) {
		return repository.findById(id).map(this::toClient);
	}

	private Client toClient(final ClientDataDto dto) {
		try {
			return factory.create(dto.id(), dto.name(), dto.cpf(), dto.email());
		} catch (final DomainException e) {
			e.printStackTrace();
			return null;
		}
	}

}
