package br.com.idogz.adapter.gateway.payment.impl;

import br.com.idogz.adapter.repository.payment.FindPaymentDataDto;
import br.com.idogz.adapter.service.FindPaymentOrderService;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.payment.FindPaymentOrderDataGateway;

public class FindPaymentOrderDataGatewayImpl implements FindPaymentOrderDataGateway {

	private final FindPaymentOrderService service;

	public FindPaymentOrderDataGatewayImpl(final FindPaymentOrderService service) {
		this.service = service;
	}

	@Override
	public FindPaymentDataDto findPayment(final String idPayment) throws DomainException {
		return execute(idPayment);
	}

	private FindPaymentDataDto execute(final String idPayment) {
		return service.findPayment(idPayment);
	}
}
