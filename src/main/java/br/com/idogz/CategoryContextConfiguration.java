package br.com.idogz;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.idogz.adapter.controller.category.CategoryController;
import br.com.idogz.adapter.controller.category.impl.CategoryControllerImpl;

import br.com.idogz.adapter.presenter.category.CategoryPresenter;
import br.com.idogz.adapter.repository.category.CategoryRepository;

import br.com.idogz.domain.factory.category.CategoryFactory;
import br.com.idogz.domain.factory.category.impl.CategoryFactoryImpl;

import br.com.idogz.domain.gateway.category.FindCategoryDataGateway;
import br.com.idogz.adapter.gateway.category.impl.FindCategoryGatewayImpl;

import br.com.idogz.domain.usecase.category.FindCategoryByIdUseCaseInteractor;
import br.com.idogz.domain.usecase.category.impl.FindCategoryUseCaseInteractorImpl;

@Configuration
public class CategoryContextConfiguration {

	@Bean
	CategoryFactory categoryFactory() {
		return new CategoryFactoryImpl();
	}

	@Bean
	FindCategoryDataGateway findCategoryDataGateway(final CategoryFactory factory, final CategoryRepository repository) {
		return new FindCategoryGatewayImpl(repository, factory);
	}

	@Bean
	FindCategoryByIdUseCaseInteractor categoryService(final FindCategoryDataGateway gateway) {
		return new FindCategoryUseCaseInteractorImpl(gateway);
	}

	@Bean
	CategoryController categoryController(final FindCategoryByIdUseCaseInteractor findCategoryUseCase, final CategoryPresenter presenter) {
		return new CategoryControllerImpl(findCategoryUseCase, presenter);
	}

}
