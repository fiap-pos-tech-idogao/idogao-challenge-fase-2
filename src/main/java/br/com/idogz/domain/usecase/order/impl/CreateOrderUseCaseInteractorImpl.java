package br.com.idogz.domain.usecase.order.impl;

import br.com.idogz.adapter.repository.payment.ConfirmPaymentDataDto;
import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.entity.OrderProduct;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.order.CreateOrderDataGateway;
import br.com.idogz.domain.gateway.order.SaveOrderProductDataGateway;
import br.com.idogz.domain.gateway.payment.SendPaymentOrderDataGateway;
import br.com.idogz.domain.usecase.TransactionManagerHelper;
import br.com.idogz.domain.usecase.order.CreateOrderUseCaseInteractor;

public class CreateOrderUseCaseInteractorImpl implements CreateOrderUseCaseInteractor {

	private final SendPaymentOrderDataGateway sendPaymentGateway;
	private final CreateOrderDataGateway createOrderGateway;
	private final SaveOrderProductDataGateway saveOrderProductGateway;
	private final TransactionManagerHelper transactionManager;

	public CreateOrderUseCaseInteractorImpl(final CreateOrderDataGateway createOrderGateway,
			final TransactionManagerHelper transactionManager, final SaveOrderProductDataGateway saveOrderProductGateway, final SendPaymentOrderDataGateway sendPaymentGateway) {
		this.createOrderGateway = createOrderGateway;
		this.saveOrderProductGateway = saveOrderProductGateway;
		this.transactionManager = transactionManager;
		this.sendPaymentGateway = sendPaymentGateway;
	}

	@Override
	public Order execute(final Order order) throws DomainException {
		transactionManager.startTransaction("createOrder");

		validateProducts(order);

		final var paymentConfirm = sendPayment(order);
		order.setUrlQrCode(paymentConfirm.point_of_interaction().transaction_data().ticket_url());
		order.setIdPayment(paymentConfirm.id());
		createOrder(order);

		transactionManager.commit();
		return order;
	}

	private void validateProducts(final Order order) throws DomainException {
		final var hasDeactivatedProduct = order.products().stream().map(OrderProduct::product)
				.anyMatch(product -> !product.active());
		if (hasDeactivatedProduct) {
			throw new DomainException("A product cannot be created with a deactivated products");
		}
	}

	private void createOrder(final Order order) throws DomainException {
		try {
			createOrderGateway.create(order);
			saveOrderProductGateway.saveAll(order.products());
		} catch (final DomainException e) {
			transactionManager.rollback();
			throw e;
		}
	}

	private ConfirmPaymentDataDto sendPayment(final Order order) throws DomainException {
		try {
			return sendPaymentGateway.sendPayment(order);
		} catch (final DomainException e) {
			transactionManager.rollback();
			throw e;
		}
	}

}
