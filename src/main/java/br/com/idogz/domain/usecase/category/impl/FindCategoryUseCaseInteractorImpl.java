package br.com.idogz.domain.usecase.category.impl;

import br.com.idogz.domain.entity.Category;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.category.CategoryIdNotFoundException;
import br.com.idogz.domain.gateway.category.FindCategoryDataGateway;
import br.com.idogz.domain.usecase.category.FindCategoryByIdUseCaseInteractor;

public class FindCategoryUseCaseInteractorImpl implements FindCategoryByIdUseCaseInteractor {

	private final FindCategoryDataGateway gateway;

	public FindCategoryUseCaseInteractorImpl(final FindCategoryDataGateway gateway) {
		this.gateway = gateway;
	}

	@Override
	public Category execute(final Integer id) throws DomainException {
		return gateway.findBy(id).orElseThrow(() -> new CategoryIdNotFoundException(id));
	}


}
