package br.com.idogz.domain.usecase.product;

import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;

public interface CreateProductUseCaseInteractor {

	Product execute(final Product dto) throws DomainException;

}
