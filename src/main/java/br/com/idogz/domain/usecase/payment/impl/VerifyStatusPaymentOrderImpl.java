package br.com.idogz.domain.usecase.payment.impl;

import java.util.UUID;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.order.OrderIdNotFoundException;
import br.com.idogz.domain.gateway.order.FindOrderByIdDataGateway;
import br.com.idogz.domain.usecase.payment.VerifyStatusPaymentOrderUserCaseInteractor;

public class VerifyStatusPaymentOrderImpl implements VerifyStatusPaymentOrderUserCaseInteractor {

	private final FindOrderByIdDataGateway findOrderByIdDataGateway;

	public VerifyStatusPaymentOrderImpl(final FindOrderByIdDataGateway findOrderByIdDataGateway) {
		this.findOrderByIdDataGateway = findOrderByIdDataGateway;
	}

	@Override
	public Order execute(final UUID idOrder) throws DomainException {
		return findOrderByIdDataGateway.findBy(idOrder).orElseThrow(() -> new OrderIdNotFoundException(idOrder));
	}

}
