package br.com.idogz.domain.usecase.order.impl;

import java.util.Comparator;
import java.util.List;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.entity.StatusOrder;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.order.FindAllOrdersDataGateway;
import br.com.idogz.domain.specification.CompositeSort;
import br.com.idogz.domain.specification.SortByAttributeAsc;
import br.com.idogz.domain.specification.SortByAttributeDesc;
import br.com.idogz.domain.specification.Specification;
import br.com.idogz.domain.specification.order.OrderStatusSpecification;
import br.com.idogz.domain.usecase.order.ListOrdersUseCaseInteractor;

public class ListOrdersUseCaseInteractorImpl implements ListOrdersUseCaseInteractor {

	private final FindAllOrdersDataGateway gateway;

	public ListOrdersUseCaseInteractorImpl(final FindAllOrdersDataGateway gateway) {
		this.gateway = gateway;
	}

	@Override
	public List<Order> execute() throws DomainException {
		final var specification = Specification.where(Specification.not(new OrderStatusSpecification(StatusOrder.AWAITING_PAYMENT)).and(Specification.not(new OrderStatusSpecification(StatusOrder.FINISHED))));
		final var sortByIdAsc = SortByAttributeAsc.of(Comparator.comparing(Order::id));
		final var sortByStatusDesc = SortByAttributeDesc.of(Comparator.comparing(Order::status));
		final var sort = CompositeSort.of(sortByStatusDesc, sortByIdAsc);
		return gateway.listAllOrders().stream().filter(specification::isSatisfiedBy).sorted(sort.getComparator()).toList();
	}

}
