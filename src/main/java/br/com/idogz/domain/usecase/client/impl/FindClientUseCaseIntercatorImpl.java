package br.com.idogz.domain.usecase.client.impl;

import br.com.idogz.domain.entity.Client;
import br.com.idogz.domain.entity.Cpf;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.client.ClientCpfNotFoundException;
import br.com.idogz.domain.gateway.client.FindClientDataGateway;
import br.com.idogz.domain.usecase.client.FindClientByCpfUseCaseInteractor;

public class FindClientUseCaseIntercatorImpl implements FindClientByCpfUseCaseInteractor {

	private final FindClientDataGateway gateway;

	public FindClientUseCaseIntercatorImpl(final FindClientDataGateway gateway) {
		this.gateway = gateway;
	}

	@Override
	public Client execute(final Cpf cpf) throws DomainException {
		final var optionalClient = gateway.find(cpf);
		return optionalClient.orElseThrow(() -> new ClientCpfNotFoundException(cpf));
	}

}
