package br.com.idogz.domain.usecase.product;

import br.com.idogz.domain.exception.DomainException;

public interface DeleteProductUseCaseInteractor {

	void execute(final int id) throws DomainException;

}
