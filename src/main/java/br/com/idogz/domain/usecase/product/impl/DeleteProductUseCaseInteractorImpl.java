package br.com.idogz.domain.usecase.product.impl;

import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.product.ProductIdNotFoundException;
import br.com.idogz.domain.gateway.product.DeleteProductDataGateway;
import br.com.idogz.domain.gateway.product.FindProductDataGateway;
import br.com.idogz.domain.usecase.product.DeleteProductUseCaseInteractor;

public class DeleteProductUseCaseInteractorImpl implements DeleteProductUseCaseInteractor {

	private final FindProductDataGateway findProductGateway;
	private final DeleteProductDataGateway deleteProductGateway;


	public DeleteProductUseCaseInteractorImpl(final DeleteProductDataGateway deleteProductGateway, final FindProductDataGateway findProductGateway) {
		this.findProductGateway = findProductGateway;
		this.deleteProductGateway = deleteProductGateway;
	}

	@Override
	public void execute(final int id) throws DomainException {
		final var deleteProduct = findProductGateway.findBy(id).orElseThrow(() -> new ProductIdNotFoundException(id));
		deleteProductGateway.delete(deleteProduct);
	}
}
