package br.com.idogz.domain.usecase.payment.impl;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.entity.StatusOrder;
import br.com.idogz.domain.entity.StatusPayment;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.order.FindOrderByIdPaymentDataGateway;
import br.com.idogz.domain.gateway.order.UpdateOrderStatusPaymentDataGateway;
import br.com.idogz.domain.gateway.payment.FindPaymentOrderDataGateway;
import br.com.idogz.domain.usecase.payment.RegisterPaymentUseCaseInteractor;

public class RegisterPaymentUseCaseInteractorImpl implements RegisterPaymentUseCaseInteractor {

	private final UpdateOrderStatusPaymentDataGateway updateOrderStatusPaymentDataGateway;

	public RegisterPaymentUseCaseInteractorImpl(final FindOrderByIdPaymentDataGateway findOrderByIdPaymentDataGateway,
			final FindPaymentOrderDataGateway findPaymentOrderDataGateway,
			final UpdateOrderStatusPaymentDataGateway updateOrderStatusPaymentDataGateway) {
		this.updateOrderStatusPaymentDataGateway = updateOrderStatusPaymentDataGateway;
	}

	@Override
	public void execute(final Order order, final StatusPayment statusPayment) throws DomainException {

		if (StatusPayment.CONFIRMED.equals(statusPayment)) {
			order.setStatusPayment(StatusPayment.CONFIRMED);
			order.setStatus(StatusOrder.RECEIVED);
			System.out.println("WEBHOOK REGISTRATION STATUS UPDATE ====>>>> [" + statusPayment.getDescription() + "]");

		} else if (StatusPayment.REFUSED.equals(statusPayment)) {
			order.setStatusPayment(StatusPayment.REFUSED);
			order.setStatus(StatusOrder.AWAITING_PAYMENT);
			System.out.println("WEBHOOK REGISTRATION STATUS UPDATE ====>>>> [" + statusPayment.getDescription() + "]");

		}
		updateOrderStatusPaymentDataGateway.updateStatusPayment(order);

	}

}
