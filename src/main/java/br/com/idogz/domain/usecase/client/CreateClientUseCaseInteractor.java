package br.com.idogz.domain.usecase.client;

import br.com.idogz.domain.entity.Client;
import br.com.idogz.domain.exception.DomainException;

public interface CreateClientUseCaseInteractor {

	Client execute(final Client dto) throws DomainException;

}
