package br.com.idogz.domain.usecase.order.impl;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.entity.StatusOrder;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.order.OrderIdNotFoundException;
import br.com.idogz.domain.gateway.order.UpdateOrderStatusDataGateway;
import br.com.idogz.domain.usecase.TransactionManagerHelper;
import br.com.idogz.domain.usecase.order.UpdateOrderStatusUseCaseInteractor;

public class UpdateOrderStatusUseCaseInteractorImpl implements UpdateOrderStatusUseCaseInteractor {

	private final UpdateOrderStatusDataGateway gateway;
	private final TransactionManagerHelper transactionManagerHelper;

	public UpdateOrderStatusUseCaseInteractorImpl(final UpdateOrderStatusDataGateway gateway,
			final TransactionManagerHelper transactionManagerHelper) {
		this.gateway = gateway;
		this.transactionManagerHelper = transactionManagerHelper;
	}

	@Override
	public void execute(final Order order, final int statusOrder) throws DomainException {
		final var statusOrderEntity = StatusOrder.findById(statusOrder);
		order.updateStatusOrder(statusOrderEntity);
		persist(order);
	}

	private void persist(final Order order) throws OrderIdNotFoundException {
		transactionManagerHelper.startTransaction("updateStatusOrder");
		gateway.updateStatus(order);
		transactionManagerHelper.commit();
	}

}
