package br.com.idogz.domain.usecase.product.impl;

import java.util.List;

import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.product.ProductIdCategoryNotFoundException;
import br.com.idogz.domain.gateway.product.FindProductDataGateway;
import br.com.idogz.domain.usecase.product.FindProductsByCategoryIdUseCaseInteractor;

public class FindProductsByCategoryIdUseCaseInteractorImpl implements FindProductsByCategoryIdUseCaseInteractor {

	private final FindProductDataGateway gateway;

	public FindProductsByCategoryIdUseCaseInteractorImpl(final FindProductDataGateway gateway) {
		this.gateway = gateway;
	}

	@Override
	public List<Product> execute(final Integer idCategory) throws DomainException {
		final var product = gateway.findByCategory(idCategory);
		if (product.size() == 0) {
			throw new ProductIdCategoryNotFoundException(idCategory);
		} else {
			return product;
		}
	}

}
