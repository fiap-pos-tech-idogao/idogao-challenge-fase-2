package br.com.idogz.domain.usecase.category;

import br.com.idogz.domain.entity.Category;
import br.com.idogz.domain.exception.DomainException;

public interface FindCategoryByIdUseCaseInteractor {

	Category execute(Integer idCategory) throws DomainException;

}
