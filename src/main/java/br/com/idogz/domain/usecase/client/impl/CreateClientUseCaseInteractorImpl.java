package br.com.idogz.domain.usecase.client.impl;

import java.util.Objects;

import br.com.idogz.domain.entity.Client;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.DuplicatedCpfException;
import br.com.idogz.domain.gateway.client.SaveClientDataGateway;
import br.com.idogz.domain.usecase.client.CreateClientUseCaseInteractor;
import br.com.idogz.domain.usecase.client.FindClientByCpfUseCaseInteractor;

public class CreateClientUseCaseInteractorImpl implements CreateClientUseCaseInteractor {

	private final FindClientByCpfUseCaseInteractor findClientUseCase;
	private final SaveClientDataGateway gateway;

	private Client newClient;
	private Client savedClient;

	public CreateClientUseCaseInteractorImpl(final SaveClientDataGateway gateway, final FindClientByCpfUseCaseInteractor findClientUseCase) {
		this.findClientUseCase = findClientUseCase;
		this.gateway = gateway;
	}

	@Override
	public Client execute(final Client client) throws DomainException {
		newClient = client;
		verifyIfCpfAlreadyExists();
		save();
		return savedClient;
	}

	private void verifyIfCpfAlreadyExists() throws DomainException {
		final var foundClient = searchClientWithCpf();
		ifClientIsNotNullThrowException(foundClient);
	}

	private Client searchClientWithCpf() {
		try {
			return findClientUseCase.execute(newClient.cpf());
		} catch (final DomainException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void ifClientIsNotNullThrowException(final Client client) throws DuplicatedCpfException {
		if (Objects.nonNull(client)) {
			throw new DuplicatedCpfException(newClient.cpf());
		}
	}

	private void save() throws DomainException {
		savedClient = gateway.save(newClient);
	}

}
