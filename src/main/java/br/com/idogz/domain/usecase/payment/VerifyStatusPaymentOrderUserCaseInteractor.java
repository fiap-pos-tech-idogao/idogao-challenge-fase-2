package br.com.idogz.domain.usecase.payment;

import java.util.UUID;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;

public interface VerifyStatusPaymentOrderUserCaseInteractor {

	Order execute(UUID idOrder) throws DomainException;

}
