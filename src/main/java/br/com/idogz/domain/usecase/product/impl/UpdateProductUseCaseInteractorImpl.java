package br.com.idogz.domain.usecase.product.impl;

import java.util.Objects;

import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.DuplicatedNameException;
import br.com.idogz.domain.exception.category.CategoryIdNotFoundException;
import br.com.idogz.domain.gateway.product.FindProductDataGateway;
import br.com.idogz.domain.gateway.product.UpdateProductDataGateway;
import br.com.idogz.domain.usecase.product.UpdateProductUseCaseInteractor;

public class UpdateProductUseCaseInteractorImpl implements UpdateProductUseCaseInteractor {

	private final FindProductDataGateway findProductDataGateway;
	private final UpdateProductDataGateway updateProductGateway;

	private Product updateProduct;

	public UpdateProductUseCaseInteractorImpl(final UpdateProductDataGateway updateProductGateway,
			final FindProductDataGateway findProductDataGateway) {
		this.findProductDataGateway = findProductDataGateway;
		this.updateProductGateway = updateProductGateway;
	}

	@Override
	public void execute(final Product product) throws DomainException {
		updateProduct = product;
		throwExceptionIfProductNameExists();
		verifyIfFoundCategory();
		update();
	}

	private void throwExceptionIfProductNameExists() throws DuplicatedNameException {
		if (findProductDataGateway.findByName(updateProduct.name()).isPresent()) {
			throw new DuplicatedNameException(updateProduct.name());
		}
	}

	private void verifyIfFoundCategory() throws DomainException {
		if (Objects.isNull(updateProduct.category())) {
			throw new CategoryIdNotFoundException();
		}
	}

	private void update() throws DomainException {
		updateProductGateway.update(updateProduct);
	}
}
