package br.com.idogz.domain.usecase.order;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;

public interface UpdateOrderStatusUseCaseInteractor {

	void execute(Order order, int statusOrder) throws DomainException;

}
