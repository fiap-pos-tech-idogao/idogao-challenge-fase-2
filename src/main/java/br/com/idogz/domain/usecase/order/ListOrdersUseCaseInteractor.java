package br.com.idogz.domain.usecase.order;

import java.util.List;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;

public interface ListOrdersUseCaseInteractor {

	List<Order> execute() throws DomainException;

}
