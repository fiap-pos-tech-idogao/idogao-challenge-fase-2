package br.com.idogz.domain.usecase.client;

import br.com.idogz.domain.entity.Client;
import br.com.idogz.domain.entity.Cpf;
import br.com.idogz.domain.exception.DomainException;

public interface FindClientByCpfUseCaseInteractor {

	Client execute(Cpf cpf) throws DomainException;

}
