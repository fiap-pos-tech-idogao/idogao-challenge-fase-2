package br.com.idogz.domain.usecase.product.impl;

import java.util.Objects;

import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.DuplicatedNameException;
import br.com.idogz.domain.exception.category.CategoryIdNotFoundException;
import br.com.idogz.domain.gateway.product.CreateProductDataGateway;
import br.com.idogz.domain.gateway.product.FindProductDataGateway;
import br.com.idogz.domain.usecase.product.CreateProductUseCaseInteractor;

public class CreateProductUseCaseInteractorImpl implements CreateProductUseCaseInteractor {

	private final FindProductDataGateway findProductDataGateway;
	private final CreateProductDataGateway createProductGateway;

	private Product newProduct;
	private Product savedProduct;

	public CreateProductUseCaseInteractorImpl(final CreateProductDataGateway createProductGateway,
			final FindProductDataGateway findProductDataGateway) {
		this.findProductDataGateway = findProductDataGateway;
		this.createProductGateway = createProductGateway;
	}

	@Override
	public Product execute(final Product product) throws DomainException {
		newProduct = product;
		throwExceptionIfNameExists();
		verifyIfFoundCategory();
		save();
		return savedProduct;
	}

	private void throwExceptionIfNameExists() throws DuplicatedNameException {
		if (findProductDataGateway.findByName(newProduct.name()).isPresent()) {
			throw new DuplicatedNameException(newProduct.name());
		}
	}

	private void verifyIfFoundCategory() throws DomainException {
		if (Objects.isNull(newProduct.category())) {
			throw new CategoryIdNotFoundException();
		}
	}

	private void save() throws DomainException {
		savedProduct = createProductGateway.save(newProduct);
	}

}
