package br.com.idogz.domain.usecase.payment;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.entity.StatusPayment;
import br.com.idogz.domain.exception.DomainException;

public interface RegisterPaymentUseCaseInteractor {

	void execute(Order order, StatusPayment statusPayment) throws DomainException;

}
