package br.com.idogz.domain.usecase.product;

import java.util.List;

import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;

public interface FindProductsByCategoryIdUseCaseInteractor {

	List<Product> execute(Integer idCategory) throws DomainException;

}
