package br.com.idogz.domain.factory.client.impl;

import br.com.idogz.domain.entity.Client;
import br.com.idogz.domain.entity.Cpf;
import br.com.idogz.domain.entity.Email;
import br.com.idogz.domain.entity.Name;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.factory.client.ClientFactory;

public class ClientFactoryImpl implements ClientFactory {

	@Override
	public Client create(String name, String cpf, String email) throws DomainException {
		final var nameVO = new Name(name);
		final var cpfVO = new Cpf(cpf);
		final var emailVO = new Email(email);
		return new Client(nameVO, cpfVO, emailVO);
	}

	@Override
	public Client create(Integer id, String name, String cpf, String email) throws DomainException {
		final var nameVO = new Name(name);
		final var cpfVO = new Cpf(cpf);
		final var emailVO = new Email(email);
		final var client = new Client(nameVO, cpfVO, emailVO);
		client.updateId(id);
		return client;
	}

}
