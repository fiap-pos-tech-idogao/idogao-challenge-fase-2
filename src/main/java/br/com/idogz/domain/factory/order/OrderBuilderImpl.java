package br.com.idogz.domain.factory.order;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

import br.com.idogz.domain.entity.Client;
import br.com.idogz.domain.entity.Notes;
import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.entity.OrderProductsList;
import br.com.idogz.domain.entity.StatusOrder;
import br.com.idogz.domain.entity.StatusPayment;
import br.com.idogz.domain.entity.TicketOrder;
import br.com.idogz.domain.entity.exception.ValidationException;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.client.FindClientDataGateway;

public class OrderBuilderImpl implements OrderBuilder {

	private static final int DEFAULT_CLIENT_ID = 1;
	private UUID orderId = UUID.randomUUID();
	private Client client;
	private LocalDate date = LocalDate.now();
	private TicketOrder ticketOrder = TicketOrder.ZERO;
	private StatusOrder statusOrder = StatusOrder.AWAITING_PAYMENT;
	private Notes notes = Notes.EMPTY;
	private OrderProductsList orderProducts = OrderProductsList.EMPTY;
	private final FindClientDataGateway gateway;
	private final OrderProductFactory orderProductFactory;
	private StatusPayment statusPayment = StatusPayment.PENDING;
	private Integer paymentId = 0;

	public OrderBuilderImpl(final FindClientDataGateway gateway, final OrderProductFactory orderProductFactory) {
		this.gateway = gateway;
		this.orderProductFactory = orderProductFactory;
	}

	@Override
	public OrderBuilder idOrder(final UUID orderId) {
		this.orderId = orderId;
		return this;
	}

	@Override
	public OrderBuilder client(final int clientId) throws DomainException {
		gateway.find(clientId).or(() -> gateway.find(OrderBuilderImpl.DEFAULT_CLIENT_ID)).ifPresent(entity -> client = entity);
		return this;
	}

	@Override
	public OrderBuilder date(final LocalDate date) {
		this.date = date;
		return this;
	}

	@Override
	public OrderBuilder ticketOrder(final int ticketOrder) throws ValidationException {
		this.ticketOrder = new TicketOrder(ticketOrder);
		return this;
	}

	@Override
	public OrderBuilder statusOrder(final int idStatus) {
		statusOrder = StatusOrder.findById(idStatus);
		return this;
	}

	@Override
	public OrderBuilder statusPayment(final int idStatusPayment) {
		statusPayment = StatusPayment.findById(idStatusPayment);
		return this;
	}

	@Override
	public OrderBuilder idPayment(final Integer paymentId) {
		this.paymentId = paymentId;
		return this;
	}

	@Override
	public OrderBuilder notes(final String notes) throws ValidationException {
		if (Objects.nonNull(notes) && !notes.isEmpty() && !notes.isBlank()) {
			this.notes = new Notes(notes);
		}
		return this;
	}

	@Override
	public OrderBuilder addProduct(final Integer productId, final int quantity, final UUID orderId, final String notes)
			throws DomainException {
		final var orderProduct = orderProductFactory.create(productId, quantity, notes, orderId);
		orderProducts = orderProducts.add(orderProduct);
		return this;
	}

	@Override
	public OrderBuilder addProduct(final int categoryId, final String categoryName, final String productName,
			final Double price, final String description, final Boolean active, final String urlImage,
			final int quantity, final String notes, final int productId) throws DomainException {
		final var orderProduct = orderProductFactory.create(categoryName, productName, price, description, active,
				urlImage, quantity, notes, null, productId, categoryId);
		orderProducts = orderProducts.add(orderProduct);
		return this;
	}

	@Override
	public Order build() throws ValidationException {
		final var order = Order.create(date, ticketOrder, statusOrder, client, notes, orderProducts, statusPayment, paymentId);
		order.updateIdWtihNotZero(orderId);
		reset();
		return order;
	}

	private void reset() {
		orderId = UUID.randomUUID();
		client = null;
		date = LocalDate.now();
		ticketOrder = TicketOrder.ZERO;
		statusOrder = StatusOrder.AWAITING_PAYMENT;
		statusPayment = StatusPayment.PENDING;
		notes = Notes.EMPTY;
		orderProducts = OrderProductsList.EMPTY;
	}

}
