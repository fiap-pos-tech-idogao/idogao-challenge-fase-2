package br.com.idogz.domain.factory.category;

import br.com.idogz.domain.entity.Category;
import br.com.idogz.domain.exception.DomainException;

public interface CategoryFactory {

	Category create(String name) throws DomainException;

	Category create(Integer id, String name) throws DomainException;

}
