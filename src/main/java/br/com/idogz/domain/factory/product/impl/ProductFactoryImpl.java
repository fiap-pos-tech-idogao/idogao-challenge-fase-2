package br.com.idogz.domain.factory.product.impl;

import br.com.idogz.domain.entity.Name;
import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.factory.product.ProductFactory;
import br.com.idogz.domain.usecase.category.FindCategoryByIdUseCaseInteractor;

public class ProductFactoryImpl implements ProductFactory {

	private final FindCategoryByIdUseCaseInteractor findCategory;

	public ProductFactoryImpl(final FindCategoryByIdUseCaseInteractor findCategory) {
		this.findCategory = findCategory;
	}

	@Override
	public Product create(final Integer idCategory, final String name, final Double price, final String description,
			final Boolean active, final String urlImage) throws DomainException {
		final var nameVO = new Name(name);
		final var category = findCategory.execute(idCategory);
		return new Product(category, nameVO, price, description, active, urlImage);
	}

	@Override
	public Product create(final Integer id, final Integer idCategory, final String name, final Double price,
			final String description, final Boolean active, final String urlImage) throws DomainException {
		final var nameVO = new Name(name);
		final var category = findCategory.execute(idCategory);
		final var product = new Product(category, nameVO, price, description, active, urlImage);
		product.updateId(id);
		return product;
	}

}
