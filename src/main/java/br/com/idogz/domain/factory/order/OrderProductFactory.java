package br.com.idogz.domain.factory.order;

import java.util.UUID;

import br.com.idogz.domain.entity.OrderProduct;
import br.com.idogz.domain.exception.DomainException;

public interface OrderProductFactory {

	OrderProduct create(final Integer productId, final int quantity, final String notes, final UUID orderId)
			throws DomainException;

	OrderProduct create(final String categoryName, final String productName, final Double price,
			final String description, final Boolean active, final String urlImage, final int quantity,
			final String notes, UUID orderId, int productId, int categoryId) throws DomainException;

}
