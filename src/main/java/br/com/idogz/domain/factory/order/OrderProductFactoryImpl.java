package br.com.idogz.domain.factory.order;

import java.util.Objects;
import java.util.UUID;

import br.com.idogz.domain.entity.Category;
import br.com.idogz.domain.entity.Name;
import br.com.idogz.domain.entity.Notes;
import br.com.idogz.domain.entity.OrderProduct;
import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.entity.exception.ValidationException;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.product.ProductIdNotFoundException;
import br.com.idogz.domain.gateway.product.FindProductDataGateway;

public class OrderProductFactoryImpl implements OrderProductFactory {

	private final FindProductDataGateway gateway;

	public OrderProductFactoryImpl(final FindProductDataGateway gateway) {
		this.gateway = gateway;
	}

	@Override
	public OrderProduct create(final Integer productId, final int quantity, final String notes, final UUID orderId)
			throws DomainException {
		final var notesVO = OrderProductFactoryImpl.toNotes(notes);
		final var product = gateway.findBy(productId).orElseThrow(() -> new ProductIdNotFoundException(productId));
		return new OrderProduct(orderId, product, quantity, notesVO);
	}

	@Override
	public OrderProduct create(final String categoryName, final String productName, final Double price,
			final String description, final Boolean active, final String urlImage, final int quantity,
			final String notes, final UUID orderId, final int productId, final int categoryId) throws DomainException {
		final var categoryVo = OrderProductFactoryImpl.createCategory(categoryName, categoryId);
		final var product = OrderProductFactoryImpl.createProduct(productName, price, description, active, urlImage,
				productId, categoryVo);
		final var notesVo = OrderProductFactoryImpl.createNotes(notes);
		return new OrderProduct(orderId, product, quantity, notesVo);
	}

	private static Category createCategory(final String categoryName, final Integer categoryId)
			throws DomainException {
		final var categoryNameVo = new Name(categoryName);
		final var categoryVo = new Category(categoryNameVo);
		categoryVo.updateId(categoryId);
		return categoryVo;
	}

	private static Product createProduct(final String productName, final Double price, final String description,
			final Boolean active, final String urlImage, final int productId, final Category categoryVo)
					throws DomainException {
		final var productNameVo = new Name(productName);
		final var product = new Product(categoryVo, productNameVo, price, description, active, urlImage);
		product.updateId(productId);
		return product;
	}

	private static Notes createNotes(final String notes) throws ValidationException {
		if (Objects.nonNull(notes) && !notes.isEmpty() && !notes.isBlank()) {
			return new Notes(notes);
		}
		return Notes.EMPTY;
	}

	private static Notes toNotes(final String notes) throws ValidationException {
		if (Objects.nonNull(notes) && !notes.isEmpty() && !notes.isBlank()) {
			return new Notes(notes);
		}
		return Notes.EMPTY;
	}
}
