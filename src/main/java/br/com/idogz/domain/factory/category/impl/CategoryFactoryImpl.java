package br.com.idogz.domain.factory.category.impl;

import br.com.idogz.domain.entity.Category;
import br.com.idogz.domain.entity.Name;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.factory.category.CategoryFactory;

public class CategoryFactoryImpl implements CategoryFactory {

	@Override
	public Category create(final String name) throws DomainException {
		final var nameVO = new Name(name);
		return new Category(nameVO);
	}

	@Override
	public Category create(final Integer id, final String name) throws DomainException {
		final var nameVO = new Name(name);
		final var category = new Category(nameVO);
		category.updateId(id);
		return category;
	}
}
