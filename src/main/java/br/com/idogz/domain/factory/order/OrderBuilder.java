package br.com.idogz.domain.factory.order;

import java.time.LocalDate;
import java.util.UUID;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.entity.exception.ValidationException;
import br.com.idogz.domain.exception.DomainException;

public interface OrderBuilder {

	OrderBuilder client(final int clientId) throws DomainException;

	OrderBuilder date(final LocalDate date);

	OrderBuilder ticketOrder(final int ticketOrder) throws ValidationException;

	OrderBuilder statusOrder(final int idStatus);

	OrderBuilder notes(final String notes) throws ValidationException;

	OrderBuilder addProduct(final Integer productId, final int quantity, final UUID orderId, String notes)
			throws DomainException;

	Order build() throws DomainException;

	OrderBuilder addProduct(final int categoryId, final String categoryName, final String productName,
			final Double price, final String description, final Boolean active, final String urlImage,
			final int quantity, final String notes, final int productId) throws DomainException;

	OrderBuilder idOrder(final UUID orderId);

	OrderBuilder idPayment(final Integer paymentId);

	OrderBuilder statusPayment(final int idStatusPayment);

}
