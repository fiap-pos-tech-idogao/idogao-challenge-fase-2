package br.com.idogz.domain.factory.product;

import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;

public interface ProductFactory {

	Product create(Integer categoryId, String name, Double price, String description, Boolean active, String urlImage)
			throws DomainException;

	Product create(Integer id, Integer categoryId, String name, Double price, String description, Boolean active,
			String urlImage) throws DomainException;
	
}
