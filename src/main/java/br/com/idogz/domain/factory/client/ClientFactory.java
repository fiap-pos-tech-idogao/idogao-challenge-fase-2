package br.com.idogz.domain.factory.client;

import br.com.idogz.domain.entity.Client;
import br.com.idogz.domain.exception.DomainException;

public interface ClientFactory {

	Client create(String name, String cpf, String email) throws DomainException;
	
	Client create(Integer id, String name, String cpf, String email) throws DomainException;
	
}
