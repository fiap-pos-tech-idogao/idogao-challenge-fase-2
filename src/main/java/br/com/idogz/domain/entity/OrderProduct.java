package br.com.idogz.domain.entity;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

import br.com.idogz.domain.entity.exception.InvalidOrderProductException;
import br.com.idogz.domain.entity.exception.ValidationException;

public class OrderProduct extends AssertionConcern implements Entity<OrderProduct> {

	private static final String ORDER_ID_CANNOT_BE_ZERO = "order id cannot be zero";
	private static final String NOTES_CANNOT_BE_NULL = "notes cannot be null";
	private static final String PRODUCT_QUANTITY_CANNOT_BE_ZERO = "product quantity cannot be zero";
	private static final String PRODUCT_ORDER_CANNOT_BE_NULL = "product order cannot be null";
	private UUID idOrder;
	private Product product;
	private int quantity;
	private Notes notes = Notes.EMPTY;

	public OrderProduct(final UUID orderId, final Product product, final int quantity, final Notes notes)
			throws ValidationException {
		setIdOrder(orderId);
		setProduct(product);
		setQuantity(quantity);
		setNotes(notes);
	}

	private void setIdOrder(final UUID orderId) {
		idOrder = orderId;
	}

	private void setProduct(final Product product) throws ValidationException {
		assertNotNull(product, new InvalidOrderProductException(OrderProduct.PRODUCT_ORDER_CANNOT_BE_NULL));
		this.product = product;
	}

	private void setQuantity(final int quantity) throws ValidationException {
		assertFalse(quantity == 0, new InvalidOrderProductException(OrderProduct.PRODUCT_QUANTITY_CANNOT_BE_ZERO));
		this.quantity = quantity;
	}

	private void setNotes(final Notes notes) throws ValidationException {
		assertNotNull(notes, new InvalidOrderProductException(OrderProduct.NOTES_CANNOT_BE_NULL));
		this.notes = notes;
	}

	public UUID idOrder() {
		return idOrder;
	}

	public int idProduct() {
		return product.id();
	}

	public int quantity() {
		return quantity;
	}

	public BigDecimal totalPrice() {
		return BigDecimal.valueOf(product.price()).multiply(new BigDecimal(quantity));
	}

	public String notes() {
		return notes.getValue();
	}

	public Product product() {
		return product;
	}

	public void updateIdOrder(final UUID id) throws ValidationException {
		assertNotNull(0, new InvalidOrderProductException(OrderProduct.ORDER_ID_CANNOT_BE_ZERO));
		setIdOrder(id);
	}

	@Override
	public boolean sameIdentityAs(final OrderProduct other) {
		return Objects.nonNull(other) && idProduct() == other.idProduct() && idOrder == other.idOrder;
	}

	@Override
	public int hashCode() {
		return Objects.hash(notes, idOrder, product, quantity);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (OrderProduct) obj;
		return sameIdentityAs(other);
	}

	@Override
	public String toString() {
		return "OrderProduct [orderId=" + idOrder + ", product=" + product + ", quantity=" + quantity + ", notes="
				+ notes + "]";
	}

}
