package br.com.idogz.domain.entity.exception;

import br.com.idogz.domain.exception.DomainException;

public class ValidationException extends DomainException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8895828432311052920L;

	public ValidationException(String message) {
		super(message);
	}

	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
