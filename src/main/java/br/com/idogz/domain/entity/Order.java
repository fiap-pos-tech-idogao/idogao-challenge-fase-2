package br.com.idogz.domain.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import br.com.idogz.domain.entity.exception.InvalidOrderException;
import br.com.idogz.domain.entity.exception.ValidationException;

public class Order extends AssertionConcern implements Entity<Order> {

	private static final String PRODUCT_ORDER_LIST_CANNOT_BE_NULL = "product order list cannot be null";
	private static final String CLIENT_ORDER_NOTES_CANNOT_BE_NULL = "client order notes cannot be null";
	private static final String CLIENT_ORDER_CANNOT_BE_NULL = "client order cannot be null";
	private static final String ORDER_STATUS_CANNOT_BE_NULL = "order status cannot be null";
	private static final String TICKET_ORDER_CANNOT_BE_NULL = "ticket order cannot be null";
	private static final String ORDER_DATE_CANNOT_BE_NULL = "order date cannot be null";
	private static final String PAYMENT_STATUS_CANNOT_BE_NULL = "payment status cannot be null";
	private static final String PAYMENT_ID_CANNOT_BE_NULL = "payment id cannot be null";
	private static final String PAYMENT_URL_QRCODE_CANNOT_BE_NULL = "payment url qrcode cannot be null";
	private UUID id;
	private LocalDate date;
	private TicketOrder ticketOrder;
	private StatusOrder statusOrder;
	private Client client;
	private Notes notesOrder;
	private OrderProductsList orderProducts;
	private StatusPayment statusPayment;

	private String urlQrcode;
	private Integer idPayment;

	private Order(final LocalDate date, final TicketOrder ticketOrder, final StatusOrder statusOrder, final Client client,
			final Notes notesOrder, final OrderProductsList orderProducts, final StatusPayment statusPayment, final Integer idPayment)
					throws ValidationException {
		setDate(date);
		setTicketOrder(ticketOrder);
		setStatus(statusOrder);
		setClient(client);
		setNotesOrder(notesOrder);
		setOrderProducts(orderProducts);
		setStatusPayment(statusPayment);
		setIdPayment(idPayment);
	}

	public static Order create(final LocalDate date, final TicketOrder ticketOrder, final StatusOrder statusOrder,
			final Client client, final Notes notesOrder, final OrderProductsList orderProducts,
			final StatusPayment statusPayment, final Integer idPayment)
					throws ValidationException {
		return new Order(date, ticketOrder, statusOrder, client, notesOrder, orderProducts, statusPayment, idPayment);
	}

	private void setDate(final LocalDate date) throws ValidationException {
		assertNotNull(date, new InvalidOrderException(Order.ORDER_DATE_CANNOT_BE_NULL));
		this.date = date;
	}

	private void setTicketOrder(final TicketOrder ticketOrder) throws ValidationException {
		assertNotNull(ticketOrder, new InvalidOrderException(Order.TICKET_ORDER_CANNOT_BE_NULL));
		this.ticketOrder = ticketOrder;
	}

	public void setStatus(final StatusOrder statusOrder) throws ValidationException {
		assertNotNull(statusOrder, new InvalidOrderException(Order.ORDER_STATUS_CANNOT_BE_NULL));
		this.statusOrder = statusOrder;
	}

	private void setClient(final Client client) throws ValidationException {
		assertNotNull(client, new InvalidOrderException(Order.CLIENT_ORDER_CANNOT_BE_NULL));
		this.client = client;
	}

	private void setNotesOrder(final Notes notesOrder) throws ValidationException {
		assertNotNull(notesOrder, new InvalidOrderException(Order.CLIENT_ORDER_NOTES_CANNOT_BE_NULL));
		this.notesOrder = notesOrder;
	}

	private void setOrderProducts(final OrderProductsList orderProducts) throws ValidationException {
		assertNotNull(orderProducts, new InvalidOrderException(Order.PRODUCT_ORDER_LIST_CANNOT_BE_NULL));
		this.orderProducts = orderProducts;
	}

	public void setStatusPayment(final StatusPayment statusPayment) throws ValidationException {
		assertNotNull(statusPayment, new InvalidOrderException(Order.PAYMENT_STATUS_CANNOT_BE_NULL));
		this.statusPayment = statusPayment;
	}

	public void setUrlQrCode(final String urlQrcode) throws ValidationException {
		assertNotNull(urlQrcode, new InvalidOrderException(Order.PAYMENT_URL_QRCODE_CANNOT_BE_NULL));
		this.urlQrcode = urlQrcode;
	}

	public void setIdPayment(final Integer idPayment) throws ValidationException {
		assertNotNull(idPayment, new InvalidOrderException(Order.PAYMENT_ID_CANNOT_BE_NULL));
		this.idPayment = idPayment;
	}

	public StatusOrder status() {
		return statusOrder;
	}

	public StatusPayment statusPayment() {
		return statusPayment;
	}

	public Notes notes() {
		return notesOrder;
	}

	public TicketOrder ticket() {
		return ticketOrder;
	}

	public Client client() {
		return client;
	}

	public LocalDate date() {
		return date;
	}

	public Integer idPayment() {
		return idPayment;
	}

	public String urlQrcode() {
		return urlQrcode;
	}

	public void updateIdWtihNotZero(final UUID id) throws ValidationException {
		if (Objects.nonNull(id)) {
			this.id = id;
			orderProducts.updateOrderId(this.id);
		}
	}

	public UUID id() {
		return id;
	}

	public List<OrderProduct> products() {
		return orderProducts.getProducts();
	}

	public BigDecimal totalValue() {
		return orderProducts.getProducts().parallelStream().map(OrderProduct::totalPrice).reduce(BigDecimal.ZERO,
				BigDecimal::add);
	}

	public void addProduct(final OrderProduct orderProduct) throws ValidationException {
		orderProducts = orderProducts.add(orderProduct);
	}

	public void addProducts(final List<OrderProduct> orderProducts) throws ValidationException {
		this.orderProducts = this.orderProducts.addAll(orderProducts);
	}

	public void updateTicket(final int ticketOrder) throws ValidationException {
		final var tickerOrderVO = new TicketOrder(ticketOrder);
		setTicketOrder(tickerOrderVO);
	}

	public void updateClient(final Client client) throws ValidationException {
		setClient(client);
	}

	public void updateStatusOrder(final StatusOrder statusOrder) throws ValidationException {
		if (StatusPayment.PENDING.equals(statusPayment)) {
			throw new ValidationException(
					"Invalid status order update. Cannot update order status when the payment still is pending");
		}
		if (!this.statusOrder.equals(statusOrder.getOldStatusOrder())) {
			throw new ValidationException("Invalid status order update. Cannot update from "
					+ this.statusOrder.getDescription() + " to " + statusOrder.getDescription());
		}
		this.statusOrder = statusOrder;
	}

	public void updateStatusPayment(final StatusPayment statusPayment) throws ValidationException {
		if (!this.statusPayment.equals(statusPayment.getOldStatusPayment())) {
			throw new ValidationException("Invalid status payment update. Cannot update from "
					+ this.statusPayment.getDescription() + " to " + this.statusPayment.getDescription());
		}
		this.statusPayment = statusPayment;
	}

	@Override
	public boolean sameIdentityAs(final Order other) {
		return Objects.nonNull(other) && id == other.id;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", date=" + date + ", ticketOrder=" + ticketOrder + ", status=" + statusOrder
				+ ", client=" + client + ", notesOrder=" + notesOrder + ", orderProducts=" + orderProducts + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(client, date, id, notesOrder, orderProducts, statusOrder, ticketOrder);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (Order) obj;
		return Objects.equals(client, other.client) && Objects.equals(date, other.date) && id == other.id
				&& Objects.equals(notesOrder, other.notesOrder) && Objects.equals(orderProducts, other.orderProducts)
				&& statusOrder == other.statusOrder && Objects.equals(ticketOrder, other.ticketOrder);
	}

}
