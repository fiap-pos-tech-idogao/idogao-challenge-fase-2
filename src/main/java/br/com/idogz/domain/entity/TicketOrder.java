package br.com.idogz.domain.entity;

import java.util.Objects;

import br.com.idogz.domain.entity.exception.InvalidTicketOrderException;
import br.com.idogz.domain.entity.exception.ValidationException;

public class TicketOrder extends AssertionConcern implements ValueObject<TicketOrder> {

	/**
	 *
	 */
	private static final long serialVersionUID = 8851419393711768801L;
	public static final TicketOrder ZERO = new TicketOrder();
	private int value;

	private TicketOrder() {
		value = 0;
	}

	public TicketOrder(final int value) throws ValidationException {
		setValue(value);
	}

	private void setValue(final int value) throws ValidationException {
		assertFalse(value == 0, new InvalidTicketOrderException("cannot be zero"));
		this.value = value;
	}

	public int value() {
		return value;
	}

	@Override
	public boolean sameValueAs(final TicketOrder other) {
		return Objects.nonNull(other) && value == other.value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (TicketOrder) obj;
		return sameValueAs(other);
	}

}
