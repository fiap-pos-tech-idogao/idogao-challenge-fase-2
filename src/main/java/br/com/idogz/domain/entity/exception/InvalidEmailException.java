package br.com.idogz.domain.entity.exception;

public class InvalidEmailException extends ValidationException {

	private static final String INVALID_EMAIL = "Invalid email: ";
	/**
	 * 
	 */
	private static final long serialVersionUID = 6842896577181661615L;

	public InvalidEmailException(String message) {
		super(INVALID_EMAIL + message);
	}

}
