package br.com.idogz.domain.entity.exception;

public class InvalidOrderProductListException extends ValidationException {

	private static final String INVALID_ORDER_PRODUCT_LIST = "Invalid order product list: ";
	/**
	 *
	 */
	private static final long serialVersionUID = 7053487908319797979L;

	public InvalidOrderProductListException(final String message) {
		super(INVALID_ORDER_PRODUCT_LIST + message);
	}

}
