package br.com.idogz.domain.entity;

public interface Entity<T> {

	boolean sameIdentityAs(T other);

}
