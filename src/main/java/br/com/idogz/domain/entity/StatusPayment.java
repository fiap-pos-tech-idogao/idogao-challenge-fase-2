package br.com.idogz.domain.entity;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import br.com.idogz.domain.entity.exception.UnexpectedStatusOrderException;

public enum StatusPayment implements ValueObject<StatusPayment> {

	PENDING(1, "Pending", null), CONFIRMED(2, "Confirmed", PENDING), REFUSED(3, "Refused", CONFIRMED);

	private final int id;
	private final String description;
	private final StatusPayment oldStatusPayment;

	StatusPayment(final int id, final String description, final StatusPayment oldStatusPayment) {
		this.id = id;
		this.description = description;
		this.oldStatusPayment = oldStatusPayment;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public StatusPayment getOldStatusPayment() {
		return oldStatusPayment;
	}

	public static StatusPayment findById(final int id) {
		return Stream.of(StatusPayment.values()).filter(StatusPayment.statusPaymentById(id)).findFirst()
				.orElseThrow(UnexpectedStatusOrderException::new);
	}

	private static Predicate<? super StatusPayment> statusPaymentById(final int id) {
		return statusPayment -> statusPayment.id == id;
	}

	@Override
	public boolean sameValueAs(final StatusPayment other) {
		return Objects.nonNull(other) && equals(other);
	}

	public static StatusPayment findByDescription(final String description) {
		return Stream.of(StatusPayment.values()).filter(StatusPayment.statusPaymentByDescription(description))
				.findFirst().orElseThrow(UnexpectedStatusOrderException::new);
	}

	private static Predicate<? super StatusPayment> statusPaymentByDescription(final String description) {
		return statusPayment -> statusPayment.description.equalsIgnoreCase(description);
	}

}
