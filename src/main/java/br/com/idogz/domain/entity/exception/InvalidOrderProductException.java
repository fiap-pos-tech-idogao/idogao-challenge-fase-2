package br.com.idogz.domain.entity.exception;

public class InvalidOrderProductException extends ValidationException {

	private static final String INVALID_ORDER_PRODUCT = "Invalid order product: ";
	/**
	 *
	 */
	private static final long serialVersionUID = -2072087533424236472L;

	public InvalidOrderProductException(final String message) {
		super(INVALID_ORDER_PRODUCT + message);
	}

}
