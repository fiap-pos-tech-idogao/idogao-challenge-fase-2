package br.com.idogz.domain.entity;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import br.com.idogz.domain.entity.exception.UnexpectedStatusOrderException;

public enum StatusOrder implements ValueObject<StatusOrder> {

	AWAITING_PAYMENT(1, "Awaiting Payment", null), RECEIVED(2, "Received", AWAITING_PAYMENT), IN_PREPARATION(3, "In Preparation", RECEIVED),
	READY(4, "Ready", IN_PREPARATION), FINISHED(5, "Finished", READY);

	private final int id;
	private final String description;
	private final StatusOrder oldStatusOrder;

	StatusOrder(final int id, final String description, final StatusOrder oldStatusOrder) {
		this.id = id;
		this.description = description;
		this.oldStatusOrder = oldStatusOrder;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public StatusOrder getOldStatusOrder() {
		return oldStatusOrder;
	}

	public static StatusOrder findById(final int id) {
		return Stream.of(StatusOrder.values()).filter(StatusOrder.statusOrderById(id)).findFirst()
				.orElseThrow(UnexpectedStatusOrderException::new);
	}

	private static Predicate<? super StatusOrder> statusOrderById(final int id) {
		return statusOrder -> statusOrder.getId() == id;
	}

	@Override
	public boolean sameValueAs(final StatusOrder other) {
		return Objects.nonNull(other) && equals(other);
	}

}
