package br.com.idogz.domain.entity.exception;

public class InvalidCpfException extends ValidationException {

	private static final String INVALID_CPF = "Invalid CPF: ";
	/**
	 * 
	 */
	private static final long serialVersionUID = 4349480187827029813L;

	public InvalidCpfException(String message) {
		super(INVALID_CPF + message);
	}
	
}
