package br.com.idogz.domain.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import br.com.idogz.domain.entity.exception.InvalidOrderProductListException;
import br.com.idogz.domain.entity.exception.ValidationException;

public class OrderProductsList extends AssertionConcern implements ValueObject<OrderProductsList> {

	private static final String PRODUCT_ORDER_CANNOT_BE_NULL = "product order cannot be null";

	private static final String PRODUCT_ORDER_LIST_CANNOT_BE_EMPTY = "product order list cannot be empty";

	/**
	 *
	 */
	private static final long serialVersionUID = -5694450317139383898L;

	public static final OrderProductsList EMPTY = new OrderProductsList();

	private List<OrderProduct> orderProducts;

	public OrderProductsList() {
		orderProducts = new ArrayList<>();
	}

	public OrderProductsList(final List<OrderProduct> orderProducts) throws ValidationException {
		assertFalse(orderProducts.isEmpty(),
				new InvalidOrderProductListException(OrderProductsList.PRODUCT_ORDER_LIST_CANNOT_BE_EMPTY));
		for (final OrderProduct orderProduct : orderProducts) {
			assertNotNull(orderProduct,
					new InvalidOrderProductListException(OrderProductsList.PRODUCT_ORDER_CANNOT_BE_NULL));
		}
		setOrderProducts(orderProducts);
	}

	private void setOrderProducts(final List<OrderProduct> orderProducts) {
		this.orderProducts = new ArrayList<>(orderProducts);
	}

	public OrderProductsList add(final OrderProduct orderProduct) throws ValidationException {
		assertNotNull(orderProduct,
				new InvalidOrderProductListException(OrderProductsList.PRODUCT_ORDER_CANNOT_BE_NULL));
		final List<OrderProduct> orderProductsExit = new ArrayList<>(orderProducts);
		orderProductsExit.add(orderProduct);
		return new OrderProductsList(orderProductsExit);
	}

	public OrderProductsList addAll(final List<OrderProduct> orderProducts) throws ValidationException {
		assertNotNull(orderProducts,
				new InvalidOrderProductListException(OrderProductsList.PRODUCT_ORDER_CANNOT_BE_NULL));
		final List<OrderProduct> orderProductsExit = new ArrayList<>(this.orderProducts);
		orderProductsExit.addAll(orderProducts);
		return new OrderProductsList(orderProductsExit);
	}

	public OrderProductsList updateOrderId(final UUID id) throws ValidationException {
		assertNotNull(id, new InvalidOrderProductListException("order id cannot be zero"));
		final var updatedIdOrderProducts = new ArrayList<OrderProduct>();
		for (final OrderProduct orderProduct : orderProducts) {
			orderProduct.updateIdOrder(id);
			updatedIdOrderProducts.add(orderProduct);
		}
		return new OrderProductsList(updatedIdOrderProducts);
	}

	public List<OrderProduct> getProducts() {
		return new ArrayList<>(orderProducts);
	}

	@Override
	public boolean sameValueAs(final OrderProductsList other) {
		return Objects.nonNull(other) && orderProducts.equals(other.orderProducts);
	}

	@Override
	public int hashCode() {
		return Objects.hash(orderProducts);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (OrderProductsList) obj;
		return sameValueAs(other);
	}

	@Override
	public String toString() {
		return orderProducts.toString();
	}

}
