package br.com.idogz.domain.entity.exception;

public class InvalidProductException extends ValidationException {

	private static final String INVALID_PRODUCT = "Invalid Product: ";
	private static final long serialVersionUID = -6955001952768956877L;

	public InvalidProductException(String message) {
		super(INVALID_PRODUCT + message);
	}

	public InvalidProductException(String message, Throwable cause) {
		super(INVALID_PRODUCT + message, cause);
	}
	
}
