package br.com.idogz.domain.gateway.client;

import br.com.idogz.domain.entity.Client;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.DataGateway;

public interface SaveClientDataGateway extends DataGateway {

	Client save(Client client) throws DomainException;

}
