package br.com.idogz.domain.gateway.order;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.DataGateway;

public interface CreateOrderDataGateway extends DataGateway {

	Order create(Order order) throws DomainException;

}
