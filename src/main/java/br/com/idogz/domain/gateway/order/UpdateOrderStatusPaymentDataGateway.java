package br.com.idogz.domain.gateway.order;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.order.OrderIdNotFoundException;

public interface UpdateOrderStatusPaymentDataGateway {

	Order updateStatusPayment(Order order) throws OrderIdNotFoundException;

}
