package br.com.idogz.domain.gateway.order;

import java.util.List;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.DataGateway;

public interface FindAllOrdersDataGateway extends DataGateway {

	public List<Order> listAllOrders() throws DomainException;

}
