package br.com.idogz.domain.gateway.category;

import java.util.Optional;

import br.com.idogz.domain.entity.Category;
import br.com.idogz.domain.gateway.DataGateway;

public interface FindCategoryDataGateway extends DataGateway {

	Optional<Category>  findBy(Integer id);

}
