package br.com.idogz.domain.gateway.product;

import java.util.List;
import java.util.Optional;

import br.com.idogz.domain.entity.Name;
import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.gateway.DataGateway;

public interface FindProductDataGateway extends DataGateway {

	Optional<Product> findByName(Name name);

	Optional<Product> findBy(int id);

	List<Product> findByCategory(int idCategory);

}
