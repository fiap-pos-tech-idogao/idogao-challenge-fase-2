package br.com.idogz.domain.gateway.client;

import java.util.Optional;

import br.com.idogz.domain.entity.Client;
import br.com.idogz.domain.entity.Cpf;
import br.com.idogz.domain.gateway.DataGateway;

public interface FindClientDataGateway extends DataGateway {

	Optional<Client>  find(Cpf cpf);

	Optional<Client>  find(int id);

}
