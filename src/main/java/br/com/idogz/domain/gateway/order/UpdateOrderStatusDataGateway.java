package br.com.idogz.domain.gateway.order;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.order.OrderIdNotFoundException;

public interface UpdateOrderStatusDataGateway {

	Order updateStatus(Order order) throws OrderIdNotFoundException;

}
