package br.com.idogz.domain.gateway.order;

import java.util.List;

import br.com.idogz.domain.entity.OrderProduct;

public interface SaveOrderProductDataGateway {

	void saveAll(List<OrderProduct> products);

}
