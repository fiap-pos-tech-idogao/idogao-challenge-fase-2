package br.com.idogz.domain.gateway.payment;

import br.com.idogz.adapter.repository.payment.ConfirmPaymentDataDto;
import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;

public interface SendPaymentOrderDataGateway {

	ConfirmPaymentDataDto sendPayment(Order order) throws DomainException;

}
