package br.com.idogz.domain.gateway.payment;

import br.com.idogz.adapter.repository.payment.FindPaymentDataDto;
import br.com.idogz.domain.exception.DomainException;

public interface FindPaymentOrderDataGateway {

	FindPaymentDataDto findPayment(String idPayment) throws DomainException;

}
