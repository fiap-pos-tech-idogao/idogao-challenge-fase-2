package br.com.idogz.domain.gateway.order;

import java.util.Optional;
import java.util.UUID;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.DataGateway;

public interface FindOrderByIdDataGateway extends DataGateway {

	Optional<Order> findBy(UUID id) throws DomainException;

}