package br.com.idogz.domain.gateway.product;

import br.com.idogz.domain.entity.Product;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.gateway.DataGateway;

public interface CreateProductDataGateway extends DataGateway {

	Product save(Product product) throws DomainException;

}
