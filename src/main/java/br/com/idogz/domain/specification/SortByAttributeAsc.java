package br.com.idogz.domain.specification;

import java.util.Comparator;

public class SortByAttributeAsc<T> implements Sort<T> {

	private final Comparator<T> comparator;

	private SortByAttributeAsc(final Comparator<T> comparator) {
		this.comparator = comparator;
	}

	public static <T> SortByAttributeAsc<T> of(final Comparator<T> comparator) {
		return new SortByAttributeAsc<>(comparator);
	}

	@Override
	public Comparator<T> getComparator() {
		return comparator;
	}

}
