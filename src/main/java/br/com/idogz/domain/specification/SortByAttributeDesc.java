package br.com.idogz.domain.specification;

import java.util.Comparator;

public class SortByAttributeDesc<T> implements Sort<T> {

	private final Comparator<T> comparator;

	private SortByAttributeDesc(final Comparator<T> comparator) {
		this.comparator = comparator.reversed();
	}

	public static <T> SortByAttributeDesc<T> of(final Comparator<T> comparator) {
		return new SortByAttributeDesc<>(comparator);
	}

	@Override
	public Comparator<T> getComparator() {
		return comparator;
	}

}
