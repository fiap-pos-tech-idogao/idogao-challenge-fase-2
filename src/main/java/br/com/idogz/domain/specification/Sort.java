package br.com.idogz.domain.specification;

import java.util.Comparator;

public interface Sort<T> {

	Comparator<T> getComparator();

}
