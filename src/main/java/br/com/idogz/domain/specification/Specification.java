package br.com.idogz.domain.specification;

public interface Specification<T> {

	boolean isSatisfiedBy(T candidate);

	static <T> Specification<T> where(final Specification<T> other) {
		return other;
	}

	default Specification<T> and(final Specification<T> other) {
		return candidate -> this.isSatisfiedBy(candidate) && other.isSatisfiedBy(candidate);
	}

	default Specification<T> or(final Specification<T> other) {
		return candidate -> this.isSatisfiedBy(candidate) || other.isSatisfiedBy(candidate);
	}

	static <T> Specification<T> not(final Specification<T> spec) {
		return candidate -> !spec.isSatisfiedBy(candidate);
	}

}
