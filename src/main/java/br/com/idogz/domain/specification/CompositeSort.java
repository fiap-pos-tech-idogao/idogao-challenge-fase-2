package br.com.idogz.domain.specification;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class CompositeSort<T> implements Sort<T> {

	private final List<Sort<T>> sorts;

	private CompositeSort(final List<Sort<T>> sorts) {
		this.sorts = sorts;
	}

	@SafeVarargs
	public static <T> Sort<T> of(final Sort<T>... sorts) {
		return new CompositeSort<>(Arrays.asList(sorts));
	}

	@Override
	public Comparator<T> getComparator() {
		return sorts.stream()
				.map(Sort::getComparator)
				.reduce(Comparator::thenComparing)
				.orElse((o1, o2) -> 0);
	}

}
