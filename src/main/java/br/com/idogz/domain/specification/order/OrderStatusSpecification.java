package br.com.idogz.domain.specification.order;

import br.com.idogz.domain.entity.Order;
import br.com.idogz.domain.entity.StatusOrder;
import br.com.idogz.domain.specification.Specification;

public class OrderStatusSpecification implements Specification<Order> {

	private final StatusOrder statusOrder;

	public OrderStatusSpecification(final StatusOrder statusOrder) {
		this.statusOrder = statusOrder;
	}

	@Override
	public boolean isSatisfiedBy(final Order order) {
		return order.status().equals(statusOrder);
	}

}
