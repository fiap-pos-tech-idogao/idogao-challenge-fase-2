package br.com.idogz.domain.exception.order;

import java.util.UUID;

import br.com.idogz.domain.exception.DomainException;

public class OrderIdNotFoundException extends DomainException {

	/**
	 *
	 */
	private static final long serialVersionUID = 2579252225932638145L;

	public OrderIdNotFoundException(final UUID id) {
		super("Order with id " + id + " not found");
	}

}
