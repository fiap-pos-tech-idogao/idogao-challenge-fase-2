package br.com.idogz.domain.exception;

import java.text.MessageFormat;

import br.com.idogz.domain.entity.Cpf;

public class DuplicatedCpfException extends DomainException {

	private static final String CPF_ALREADY_EXISTS = "CPF already exists: {0}";
	/**
	 *
	 */
	private static final long serialVersionUID = 9189556126034136467L;

	public DuplicatedCpfException(final Cpf cpf) {
		super(MessageFormat.format(DuplicatedCpfException.CPF_ALREADY_EXISTS, cpf.value()));
	}

}
