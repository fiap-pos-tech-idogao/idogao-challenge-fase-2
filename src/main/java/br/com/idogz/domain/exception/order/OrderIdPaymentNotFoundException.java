package br.com.idogz.domain.exception.order;

import br.com.idogz.domain.exception.DomainException;

public class OrderIdPaymentNotFoundException extends DomainException {

	/**
	 *
	 */
	private static final long serialVersionUID = 2579252225932638145L;

	public OrderIdPaymentNotFoundException(final Integer idPayment) {
		super("Order with id payment " + idPayment + " not found");
	}

}
