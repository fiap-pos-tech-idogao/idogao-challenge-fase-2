package br.com.idogz.domain.exception.payment;

import br.com.idogz.domain.exception.DomainException;

public class InvalidStatusPaymentException extends DomainException {

	/**
	 *
	 */
	private static final long serialVersionUID = 2579252225932638145L;

	public InvalidStatusPaymentException(final String statusPayment) {
		super("It is impossible to register again as it is already registered as" + statusPayment);
	}

}
