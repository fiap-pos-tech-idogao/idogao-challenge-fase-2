package br.com.idogz.domain.exception.product;

import br.com.idogz.domain.exception.DomainException;

public class ProductNameExistingException extends DomainException {

	private static final long serialVersionUID = 6232136759238965693L;

	public ProductNameExistingException(String name) {
		super("Product with name " + name + " already exists");
	}
}
