package br.com.idogz.domain.exception.category;

import java.text.MessageFormat;

import br.com.idogz.domain.exception.DomainException;

public class CategoryIdNotFoundException extends DomainException {

	private static final String CATEGORY_WITH_ID_NOT_FOUND = "Category with Id {0} not found";
	private static final String CATEGORY_NOT_FOUND = "Category not found";
	private static final long serialVersionUID = 6232136759238965693L;

	public CategoryIdNotFoundException(final Integer categoryId) {
		super(MessageFormat.format(CategoryIdNotFoundException.CATEGORY_WITH_ID_NOT_FOUND, categoryId));
	}

	public CategoryIdNotFoundException() {
			super(CategoryIdNotFoundException.CATEGORY_NOT_FOUND);
	}
}
