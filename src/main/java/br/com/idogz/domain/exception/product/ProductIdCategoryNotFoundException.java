package br.com.idogz.domain.exception.product;

import br.com.idogz.domain.exception.DomainException;

public class ProductIdCategoryNotFoundException extends DomainException {

	private static final long serialVersionUID = 6232136759238965693L;

	public ProductIdCategoryNotFoundException(Integer idCategory) {
		super("Product with Id Category " + idCategory + " not found");
	}
}
