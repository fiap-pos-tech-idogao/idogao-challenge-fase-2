package br.com.idogz.domain.exception.payment;

import br.com.idogz.domain.exception.DomainException;

public class InvalidTypePaymentException extends DomainException {

	/**
	 *
	 */
	private static final long serialVersionUID = 2579252225932638145L;

	public InvalidTypePaymentException(final String typePayment) {
		super("Invalid type " + typePayment);
	}

}
