package br.com.idogz.domain.exception.product;

import br.com.idogz.domain.exception.DomainException;

public class ProductIdNotFoundException extends DomainException {

	private static final long serialVersionUID = 6232136759238965693L;

	public ProductIdNotFoundException(Integer idProduct) {
		super("Product with Id " + idProduct + " not found");
	}
}
