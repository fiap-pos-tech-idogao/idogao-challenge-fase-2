package br.com.idogz.domain.exception.payment;

import br.com.idogz.domain.exception.DomainException;

public class InvalidUserMercadoPagoException extends DomainException {

	/**
	 *
	 */
	private static final long serialVersionUID = 2579252225932638145L;

	public InvalidUserMercadoPagoException(final String idUser) {
		super("Invalid user " + idUser + " Mercado Pago");
	}

}
