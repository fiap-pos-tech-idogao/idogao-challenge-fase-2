package br.com.idogz.domain.exception.category;

import java.text.MessageFormat;

import br.com.idogz.domain.entity.Name;
import br.com.idogz.domain.exception.DomainException;


public class DuplicatedNameException extends DomainException {

	private static final String NAME_ALREADY_EXISTS = "Name already exists: {0}";
	/**
	 *
	 */
	private static final long serialVersionUID = 9189556126034136467L;

	public DuplicatedNameException(final Name name) {
		super(MessageFormat.format(DuplicatedNameException.NAME_ALREADY_EXISTS, name.value()));
	}

}
