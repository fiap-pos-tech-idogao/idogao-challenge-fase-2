package br.com.idogz.domain.exception.product;

import br.com.idogz.domain.entity.Name;
import br.com.idogz.domain.exception.DomainException;

public class ProductNameNotFoundException extends DomainException {

	private static final long serialVersionUID = 6232136759238965693L;

	public ProductNameNotFoundException(Name name) {
		super("Product with name " + name.value() + " not found");
	}
}
