package br.com.idogz;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "api.mercadopago") 
public class ApiMercadoPagoPropertiesFactory {

    private String urlCreatePayment;
    private String urlFindPayment;
    private String urlWebhookPayment;
    private String accessToken;
    private String secret;
    private String userId;
    private String paymentMethodId;
    private String type;
    private int installments;
    
    public String urlCreatePayment() {
        return urlCreatePayment;
    }

    public void setUrlCreatePayment(final String urlCreatePayment) {
        this.urlCreatePayment = urlCreatePayment;
    }

    public String urlFindPayment() {
        return urlFindPayment;
    }

    public void setUrlFindPayment(final String urlFindPayment) {
        this.urlFindPayment = urlFindPayment;
    }

    public String urlWebhookPayment() {
        return urlWebhookPayment;
    }

    public void setUrlWebhookPayment(final String urlWebhookPayment) {
        this.urlWebhookPayment = urlWebhookPayment;
    }

    public String accessToken() {
        return accessToken;
    }

    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }

    public String secret() {
        return secret;
    }

    public void setSecret(final String secret) {
        this.secret = secret;
    }

    public String userId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String paymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(final String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String type() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public int installments() {
        return installments;
    }

    public void setInstallments(final int installments) {
        this.installments = installments;
    }
}