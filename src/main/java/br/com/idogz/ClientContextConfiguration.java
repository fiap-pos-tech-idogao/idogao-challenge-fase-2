package br.com.idogz;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.idogz.adapter.controller.client.ClientController;
import br.com.idogz.adapter.controller.client.impl.ClientControllerImpl;
import br.com.idogz.adapter.gateway.client.impl.FindClientGatewayImpl;
import br.com.idogz.adapter.gateway.client.impl.SaveClientGatewayImpl;
import br.com.idogz.adapter.presenter.client.ClientPresenter;
import br.com.idogz.adapter.repository.client.ClientRepository;
import br.com.idogz.domain.factory.client.ClientFactory;
import br.com.idogz.domain.factory.client.impl.ClientFactoryImpl;
import br.com.idogz.domain.gateway.client.FindClientDataGateway;
import br.com.idogz.domain.gateway.client.SaveClientDataGateway;
import br.com.idogz.domain.usecase.client.CreateClientUseCaseInteractor;
import br.com.idogz.domain.usecase.client.FindClientByCpfUseCaseInteractor;
import br.com.idogz.domain.usecase.client.impl.CreateClientUseCaseInteractorImpl;
import br.com.idogz.domain.usecase.client.impl.FindClientUseCaseIntercatorImpl;
import br.com.idogz.external.repository.client.ClientJpaRepository;
import br.com.idogz.external.repository.client.ClientRepositoryImpl;

@Configuration
public class ClientContextConfiguration {

	@Bean
	ClientRepository clientRepository(final ClientJpaRepository repository) {
		return new ClientRepositoryImpl(repository);
	}

	@Bean
	ClientFactory clientFactory() {
		return new ClientFactoryImpl();
	}

	@Bean
	SaveClientDataGateway saveClientDataGateway(final ClientRepository repository, final ClientFactory factory) {
		return new SaveClientGatewayImpl(repository, factory);
	}

	@Bean
	FindClientDataGateway findClientDataGateway(final ClientFactory factory, final ClientRepository repository) {
		return new FindClientGatewayImpl(repository, factory);
	}

	@Bean
	CreateClientUseCaseInteractor createClientUseCaseInteractor(final SaveClientDataGateway gateway, final FindClientByCpfUseCaseInteractor findClientUseCase) {
		return new CreateClientUseCaseInteractorImpl(gateway, findClientUseCase);
	}

	@Bean
	FindClientByCpfUseCaseInteractor clientService(final FindClientDataGateway gateway) {
		return new FindClientUseCaseIntercatorImpl(gateway);
	}

	@Bean
	ClientController clientController(final FindClientByCpfUseCaseInteractor findClientUseCase, final ClientPresenter presenter, final CreateClientUseCaseInteractor createClientUseCase, final ClientFactory factory) {
		return new ClientControllerImpl(findClientUseCase, presenter, createClientUseCase, factory);
	}

}
