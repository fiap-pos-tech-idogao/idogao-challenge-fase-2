package br.com.idogz.external.webhook;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.idogz.adapter.controller.webhook.WebhookController;
import br.com.idogz.adapter.controller.webhook.WebhookRegisterPaymentDto;
import br.com.idogz.adapter.controller.webhook.WebhookRegisterPaymentDto.WebhookRegisterPaymentDataDto;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.external.api.error.ApiError;
import br.com.idogz.external.modelassembler.order.OrderModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Idogz Pago Webhook", description = "Webhook to update payment status by Idogz Pago")
@RestController
@RequestMapping(path = "/webhook-idogz-pago")
public class IdogzPagoWebhook {

	private final WebhookController controller;

	public IdogzPagoWebhook(final WebhookController idogzPagoWebhookController) {
		controller = idogzPagoWebhookController;
	}

	@PostMapping
	@Operation(summary = "Webhook notification event payment", description = "Records payment event for the order", tags = {
	"post" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Order payment event registered successfully", content = @Content(mediaType = "application/json", schema = @Schema(implementation = OrderModel.class))),
			@ApiResponse(responseCode = "400", description = "Payment event for the failed registered order", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))) })
	public ResponseEntity<Object> register(@RequestBody final IdogzPagoWebhookRegisterPaymentRequest request)
			throws DomainException {

		System.out.println("WEBHOOK REGISTRATION IS UP ====>>>> [" + request + "]");

		try {
			final var whRegisterPaymentDto = new WebhookRegisterPaymentDto(request.paymentStatus(), "", "",
					new WebhookRegisterPaymentDataDto(request.data().id()));

			controller.registerPayment(whRegisterPaymentDto);
			return ResponseEntity.ok().build();

		} catch (final Exception exception) {
			System.out.println("WEBHOOK REGISTRATION ERROR ====>>>> [" + exception + "]");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}

	}

}
