package br.com.idogz.external.webhook;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Registration of webhook payment events")
public record MercadoPagoWebhookRegisterPaymentRequest(
		@Schema(description = "Registered update action", example = "payment.created")
		String action,
		@Schema(description = "Kind of event", example = "payment")
		String type,
		@Schema(description = "User identification on Mercado Livre", example = "43874879")
		String user_id,
		WebhookRegisterDataRequest data) {
	public record WebhookRegisterDataRequest(
					@Schema(description = "Payment identification on Mercado Livre", example = "1318902942")
			Integer id) {
	}
}
