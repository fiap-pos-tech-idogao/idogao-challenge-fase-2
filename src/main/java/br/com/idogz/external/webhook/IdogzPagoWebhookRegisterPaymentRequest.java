package br.com.idogz.external.webhook;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Registration of webhook payment events")
public record IdogzPagoWebhookRegisterPaymentRequest(
		@Schema(description = "Payment Status", example = "Confirmed")
		String paymentStatus,
		WebhookRegisterDataRequest data) {
	public record WebhookRegisterDataRequest(
			@Schema(description = "Payment ID", example = "1318902942")
			Integer id) {
	}
}
