package br.com.idogz.external.repository.order;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

import br.com.idogz.adapter.gateway.order.impl.UpdateStatusOrderDataDto;
import br.com.idogz.adapter.gateway.order.impl.UpdateStatusPaymentOrderDataDto;
import br.com.idogz.adapter.repository.category.PersistedCategoryDto;
import br.com.idogz.adapter.repository.client.ClientDataDto;
import br.com.idogz.adapter.repository.order.CreateOrderDataDto;
import br.com.idogz.adapter.repository.order.OrderDataDto;
import br.com.idogz.adapter.repository.order.OrderDataDto.OrderProductDataDto;
import br.com.idogz.adapter.repository.order.OrderIDTicketDataView;
import br.com.idogz.adapter.repository.order.OrderRepository;
import br.com.idogz.adapter.repository.product.ProductDto;
import br.com.idogz.external.repository.order.entity.OrderEntity;
import br.com.idogz.external.repository.order.entity.OrderProductEntity;
import br.com.idogz.external.repository.order.entity.StatusOrderEntity;
import br.com.idogz.external.repository.payment.entity.StatusPaymentEntity;

public class OrderRepositoryImpl implements OrderRepository {

	private final OrderJpaRepository repository;

	public OrderRepositoryImpl(final OrderJpaRepository repository) {
		this.repository = repository;
	}

	@Override
	public OrderIDTicketDataView save(final CreateOrderDataDto dto) {
		return repository.save(dto.id(), dto.date(), dto.totalValue(), dto.idStatus(), dto.idClient(), dto.notes(),
				dto.idStatusPayment(), dto.idPayment());
	}

	@Override
	public void updateStatus(final UpdateStatusOrderDataDto dto) {
		final var orderEntity = repository.findById(dto.id()).get();
		orderEntity.setStatus(new StatusOrderEntity(dto.status(), null));
		repository.save(orderEntity);
	}

	@Override
	public void updateStatusPayment(final UpdateStatusPaymentOrderDataDto dto) {
		final var orderEntity = repository.findById(dto.id()).get();
		orderEntity.setStatus(new StatusOrderEntity(dto.status(), null));
		orderEntity.setStatusPayment(new StatusPaymentEntity(dto.statusPayment(), null));
		repository.save(orderEntity);
	}

	@Override
	public List<OrderDataDto> findAll() {
		return repository.findAll().stream().map(toOrderDto()).toList();
	}

	@Override
	public Optional<OrderDataDto> findBy(final UUID id) {
		return repository.findById(id).map(toOrderDto());
	}

	@Override
	public Optional<OrderDataDto> findByIdPayment(final Integer idPayment) {
		return repository.getOrderByIdPayment(idPayment).map(toOrderDto());
	}

	private Function<? super OrderEntity, OrderDataDto> toOrderDto() {
		return entity -> {
			final var clientEntity = entity.client();
			final var clientDto = new ClientDataDto(clientEntity.id(), clientEntity.name(), clientEntity.cpf(),
					clientEntity.email());
			final var orderProductDtos = entity.orderProducts().stream().map(toOrderProducDto())
					.toList();
			return new OrderDataDto(entity.id(), entity.dateOrder(), entity.ticketOrder(), entity.status().getId(), 
					entity.getStatusPayment().getIdStatus(), entity.idPayment(),
					clientDto, entity.notesOrder(), orderProductDtos);
		};
	}

	private Function<? super OrderProductEntity, OrderProductDataDto> toOrderProducDto() {
		return entity -> {
			final var productEntity = entity.id().product();
			final var categoryEntity = productEntity.category();
			final var categoryDto = new PersistedCategoryDto(categoryEntity.id(), categoryEntity.name());
			final var productDto = new ProductDto(productEntity.id(), categoryDto, productEntity.name(),
					productEntity.price(), productEntity.description(),
					productEntity.active(), productEntity.urlImage());
			return new OrderProductDataDto(productDto, entity.quantity(), entity.notes());
		};
	}

}
