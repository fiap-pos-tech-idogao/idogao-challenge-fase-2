package br.com.idogz.external.repository.product;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductJpaRepository extends JpaRepository<ProductEntity, Integer> {

	@Query(value = "select * from get_product_by_name(:_name)", nativeQuery = true)
	Optional<ProductEntity> findProductByName(@Param("_name") String name);

	@Query(value = "select * from get_product_by_id(:_productid)", nativeQuery = true)
	Optional<ProductEntity> findProductById(@Param("_productid") Integer productId);

	@Query(value = "select * from get_product_by_categoryid(:_categoryid)", nativeQuery = true)
	List<ProductEntity> findProductByCategoryId(@Param("_categoryid") Integer categoryId);

}
