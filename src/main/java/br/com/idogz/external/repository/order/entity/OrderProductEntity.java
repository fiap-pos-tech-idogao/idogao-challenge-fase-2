package br.com.idogz.external.repository.order.entity;

import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "tbOrderProduct", schema = "public")
public class OrderProductEntity {

	@EmbeddedId
	private IdOrderProduct id;

	@Column(nullable = false)
	private Integer quantity;

	@Column(length = 200)
	private String notes;

	public OrderProductEntity() {
	}

	public OrderProductEntity(final IdOrderProduct id, final Integer quantity, final String notes) {
		this.id = id;
		this.quantity = quantity;
		this.notes = notes;
	}

	public Integer quantity() {
		return quantity;
	}

	public void setQuantity(final Integer quantity) {
		this.quantity = quantity;
	}

	public String notes() {
		return notes;
	}

	public void setNotes(final String notes) {
		this.notes = notes;
	}

	public IdOrderProduct id() {
		return id;
	}

	public void setId(final IdOrderProduct id) {
		this.id = id;
	}

	public int getIdProduct() {
		return id.product().id();
	}

	public UUID getIdOrder() {
		return id.getOrder().id();
	}

}
