package br.com.idogz.external.repository.payment.entity;

import java.util.Set;

import br.com.idogz.external.repository.order.entity.OrderEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "tbStatusPayment", schema = "public")
public class StatusPaymentEntity {

	@Id
	@Column(nullable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer idStatus;

	@Column(nullable = false, unique = true, length = 100)
	private String description;

	@OneToMany(mappedBy = "statusPayment")
	private Set<OrderEntity> orders;

	public StatusPaymentEntity() {
	}

	public StatusPaymentEntity(final Integer idStatus, final String description) {
		this.idStatus = idStatus;
		this.description = description;
	}

	public Integer getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(final Integer idStatus) {
		this.idStatus = idStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public Set<OrderEntity> getOrders() {
		return orders;
	}

	public void setOrders(final Set<OrderEntity> orders) {
		this.orders = orders;
	}

}
