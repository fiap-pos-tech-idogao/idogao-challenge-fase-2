package br.com.idogz.external.repository.order;

import br.com.idogz.adapter.gateway.order.impl.OrderProductDataDto;
import br.com.idogz.adapter.repository.order.OrderProductRepository;

public class OrderProductRepositoryImpl implements OrderProductRepository {

	private final OrderProductJpaRepository repository;

	public OrderProductRepositoryImpl(final OrderProductJpaRepository repository) {
		this.repository = repository;
	}

	@Override
	public void saveOrderProduct(final OrderProductDataDto dto) {
		repository.saveOrderProduct(dto.idOrder(), dto.idProduct(), dto.quantity(), dto.notes());
	}

}
