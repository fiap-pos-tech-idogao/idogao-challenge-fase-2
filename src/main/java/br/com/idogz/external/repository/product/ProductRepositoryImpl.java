package br.com.idogz.external.repository.product;

import java.util.List;
import java.util.Optional;

import br.com.idogz.adapter.repository.product.CreateProductDataDto;
import br.com.idogz.adapter.repository.product.ProductDataDto;
import br.com.idogz.adapter.repository.product.ProductRepository;
import br.com.idogz.external.repository.category.CategoryEntity;

public class ProductRepositoryImpl implements ProductRepository {

	private final ProductJpaRepository repository;

	public ProductRepositoryImpl(final ProductJpaRepository repository) {
		this.repository = repository;
	}

	@Override
	public ProductDataDto create(final CreateProductDataDto dto) {
		final var entity = toEntity(dto);
		final var productEntity = repository.save(entity);
		return toProductDataDto(productEntity);
	}

	@Override
	public void delete(final CreateProductDataDto dto) {
		final var entity = toEntity(dto);
		repository.save(entity);
	}

	@Override
	public Optional<ProductDataDto> getProductByName(final String name) {
		return repository.findProductByName(name).map(this::toProductDataDto);
	}

	@Override
	public Optional<ProductDataDto> getProductById(final Integer productId) {
		return repository.findProductById(productId).map(this::toProductDataDto);
	}

	@Override
	public List<ProductDataDto> getProductByCategoryId(final Integer categoryId) {
		return repository.findProductByCategoryId(categoryId).stream().map(this::toProductDataDto).toList();
	}

	private ProductEntity toEntity(final CreateProductDataDto dto) {
		return new ProductEntity(dto.id(), new CategoryEntity(dto.categoryId(), ""), dto.name(),
				dto.price(), dto.description(), dto.active(), dto.urlImage());
	}

	private ProductDataDto toProductDataDto(final ProductEntity entity) {
		return new ProductDataDto(entity.id(), entity.category().id(), entity.name(), entity.price(),
				entity.description(), entity.active(), entity.urlImage());
	}

	@Override
	public void update(final ProductDataDto dto) {
		final var entity = toEntity(dto);
		repository.save(entity);
	}

	private ProductEntity toEntity(final ProductDataDto dto) {
		return new ProductEntity(dto.id(), new CategoryEntity(dto.categoryId(), ""), dto.name(), dto.price(),
				dto.description(), dto.active(), dto.urlImage());
	}

}
