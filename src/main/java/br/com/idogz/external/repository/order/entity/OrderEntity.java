package br.com.idogz.external.repository.order.entity;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

import br.com.idogz.external.repository.client.ClientEntity;
import br.com.idogz.external.repository.payment.entity.StatusPaymentEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "tbOrder")
public class OrderEntity {

	@Id
	@Column(nullable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.UUID)
	private UUID idOrder;

	@Column(nullable = false)
	private Integer ticketOrder;

	@Column(nullable = false)
	private LocalDate dateOrder;

	@Column(nullable = false)
	private Double totalValueOrder;

	@Column(length = 100)
	private String notesOrder;

	@Column(nullable = false)
	private Integer idPayment;

	@ManyToOne()
	@JoinColumn(name = "idstatus", nullable = false)
	private StatusOrderEntity status;

	@ManyToOne()
	@JoinColumn(name = "idstatuspayment", nullable = false)
	private StatusPaymentEntity statusPayment;

	@ManyToOne
	@JoinColumn(name = "idclient", nullable = false)
	private ClientEntity client;

	@OneToMany(mappedBy = "id.order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<OrderProductEntity> orderProducts;

	public OrderEntity() {
	}

	public OrderEntity(final UUID idOrder, final Integer ticketOrder, final LocalDate dateOrder,
			final Double totalValueOrder,
			final String notesOrder, final StatusOrderEntity status, 
			final StatusPaymentEntity statusPayment, final Integer idPayment,
			final ClientEntity client,
			final Set<OrderProductEntity> orderProducts) {
		this.idOrder = idOrder;
		this.ticketOrder = ticketOrder;
		this.dateOrder = dateOrder;
		this.totalValueOrder = totalValueOrder;
		this.notesOrder = notesOrder;
		this.status = status;
		this.client = client;
		this.orderProducts = orderProducts;
		this.statusPayment = statusPayment;
	}

	public StatusPaymentEntity getStatusPayment() {
		return statusPayment;
	}

	public void setStatusPayment(final StatusPaymentEntity statusPayment) {
		this.statusPayment = statusPayment;
	}

	public UUID id() {
		return idOrder;
	}

	public void setIdOrder(final UUID idOrder) {
		this.idOrder = idOrder;
	}

	public Integer ticketOrder() {
		return ticketOrder;
	}

	public void setTicketOrder(final Integer ticketOrder) {
		this.ticketOrder = ticketOrder;
	}

	public LocalDate dateOrder() {
		return dateOrder;
	}

	public void setDateOrder(final LocalDate dateOrder) {
		this.dateOrder = dateOrder;
	}

	public Double totalValueOrder() {
		return totalValueOrder;
	}

	public void setTotalValueOrder(final Double totalValueOrder) {
		this.totalValueOrder = totalValueOrder;
	}

	public String notesOrder() {
		return notesOrder;
	}

	public void setNotesOrder(final String notesOrder) {
		this.notesOrder = notesOrder;
	}

	public StatusOrderEntity status() {
		return status;
	}

	public void setStatus(final StatusOrderEntity status) {
		this.status = status;
	}

	public Integer idPayment() {
		return idPayment;
	}

	public void setIdPayment(final Integer idPayment) {
		this.idPayment = idPayment;
	}

	public ClientEntity client() {
		return client;
	}

	public void setClient(final ClientEntity client) {
		this.client = client;
	}

	public Set<OrderProductEntity> orderProducts() {
		return orderProducts;
	}

	public void setOrderProducts(final Set<OrderProductEntity> orderProducts) {
		this.orderProducts = orderProducts;
	}

}
