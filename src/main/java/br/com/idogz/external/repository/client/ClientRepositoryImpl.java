package br.com.idogz.external.repository.client;

import java.util.Optional;
import java.util.function.Function;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.idogz.adapter.repository.client.ClientDataDto;
import br.com.idogz.adapter.repository.client.ClientRepository;
import br.com.idogz.adapter.repository.client.CreateClientDataDto;
import br.com.idogz.domain.entity.Cpf;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.domain.exception.DuplicatedCpfException;

public class ClientRepositoryImpl implements ClientRepository {

	private static final String ERROR_DUPLICATE_KEY_VALUE_VIOLATES_UNIQUE_CONSTRAINT = "ERROR: duplicate key value violates unique constraint";
	private final ClientJpaRepository repository;

	public ClientRepositoryImpl(final ClientJpaRepository repository) {
		this.repository = repository;
	}

	@Override
	public ClientDataDto save(final CreateClientDataDto dto) throws DomainException {
		final var entity = toEntity(dto);
		final var savedEntity = save(entity);
		return toDto(savedEntity);
	}

	private ClientEntity toEntity(final CreateClientDataDto dto) {
		return new ClientEntity(null, dto.name(), dto.cpf(), dto.email());
	}

	private ClientEntity save(final ClientEntity entity) throws DomainException {
		try {
			return repository.save(entity);
		} catch (final DataIntegrityViolationException e) {
			throw ClientRepositoryImpl.handlerException(entity, e);
		}
	}

	private static DomainException handlerException(final ClientEntity entity, final DataIntegrityViolationException e)
			throws DomainException {
		final var message = e.getMostSpecificCause().getMessage();
		if (message.contains(ClientRepositoryImpl.ERROR_DUPLICATE_KEY_VALUE_VIOLATES_UNIQUE_CONSTRAINT)) {
			return new DuplicatedCpfException(new Cpf(entity.cpf()));
		}
		return new DomainException(message);
	}

	private ClientDataDto toDto(final ClientEntity savedEntity) {
		return new ClientDataDto(savedEntity.id(), savedEntity.name(), savedEntity.cpf(), savedEntity.email());
	}

	@Override
	public Optional<ClientDataDto> getClientByCpf(final String cpf) {
		return repository.getClientByCpf(cpf)
				.map(toClientDto());
	}

	@Override
	public Optional<ClientDataDto> findById(final int id) {
		return repository.findById(id).map(toClientDto());
	}

	private Function<? super ClientEntity, ClientDataDto> toClientDto() {
		return entity -> new ClientDataDto(entity.id(), entity.name(), entity.cpf(), entity.email());
	}

}
