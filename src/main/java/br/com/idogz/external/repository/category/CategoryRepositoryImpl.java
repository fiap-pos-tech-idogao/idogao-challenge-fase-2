package br.com.idogz.external.repository.category;

import java.util.Optional;

import org.springframework.stereotype.Component;

import br.com.idogz.adapter.repository.category.PersistedCategoryDto;
import br.com.idogz.adapter.repository.category.CategoryRepository;

@Component
public class CategoryRepositoryImpl implements CategoryRepository {

	private final CategoryJpaRepository repository;

	public CategoryRepositoryImpl(final CategoryJpaRepository repository) {
		this.repository = repository;
	}

	@Override
	public Optional<PersistedCategoryDto> findById(final Integer idCategory) {
		return repository.findById(idCategory).map(entity -> new PersistedCategoryDto(entity.id(), entity.name()));
	}

}
