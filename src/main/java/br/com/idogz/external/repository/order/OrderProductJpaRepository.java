package br.com.idogz.external.repository.order;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.idogz.external.repository.order.entity.IdOrderProduct;
import br.com.idogz.external.repository.order.entity.OrderProductEntity;

public interface OrderProductJpaRepository extends JpaRepository<OrderProductEntity, IdOrderProduct> {

	@Query(value = "SELECT * FROM public.create_order_product(:_idorder, :_idproduct, :_quantity, :_notes);", nativeQuery = true)
	void saveOrderProduct(@Param("_idorder") UUID idOrder, @Param("_idproduct") int idProduct,
			@Param("_quantity") int quantity, @Param("_notes") String notes);

}
