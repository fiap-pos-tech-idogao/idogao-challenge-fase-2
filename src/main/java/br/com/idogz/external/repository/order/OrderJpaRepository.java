package br.com.idogz.external.repository.order;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.idogz.adapter.repository.order.OrderIDTicketDataView;
import br.com.idogz.external.repository.order.entity.OrderEntity;

public interface OrderJpaRepository extends JpaRepository<OrderEntity, UUID>, JpaSpecificationExecutor<OrderEntity> {

	@Query(value = "select * FROM public.create_order(:_idorder, :_dateorder, :_totalvalueorder, :_idstatus, :_idstatuspayment, :_idclient, :_notesorder, :_idpayment);", nativeQuery = true)
	OrderIDTicketDataView save(@Param("_idorder") UUID idOrder, @Param("_dateorder") LocalDate date,
			@Param("_totalvalueorder") Double totalvalueorder, @Param("_idstatus") Integer idstatus,
			@Param("_idclient") Integer idclient, @Param("_notesorder") String notesorder,
			@Param("_idstatuspayment") Integer idStatusPayment, @Param("_idpayment") Integer idpayment);

	@Query(value = "select * from get_order_by_paymentid(:_paymentid)", nativeQuery = true)
	Optional<OrderEntity> getOrderByIdPayment(@Param("_paymentid") Integer idPayment);

}
