package br.com.idogz.external.repository.category;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CategoryJpaRepository extends JpaRepository<CategoryEntity, Integer> {

	@Override
	@Query(value = "select * from get_category_by_id(:_categoryid)", nativeQuery = true)
	Optional<CategoryEntity> findById(@Param("_categoryid") Integer categoryId);
}
