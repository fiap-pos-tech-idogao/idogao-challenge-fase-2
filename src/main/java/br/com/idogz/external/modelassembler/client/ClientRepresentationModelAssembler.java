package br.com.idogz.external.modelassembler.client;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.idogz.adapter.presenter.client.ClientPresenter;
import br.com.idogz.domain.entity.Client;
import br.com.idogz.external.api.client.ClientApi;

@Component
public class ClientRepresentationModelAssembler extends RepresentationModelAssemblerSupport<Client, ClientModel> implements ClientPresenter {

	public ClientRepresentationModelAssembler() {
		super(ClientApi.class, ClientModel.class);
	}

	@Override
	public ClientModel toModel(final Client dto) {
		final var model = new ClientModel(dto.id(), dto.name(), dto.unformattingCpf(),
				dto.emailAddress());

		model.add(Link.of("/client/{cpf}").withRel(LinkRelation.of("client")).expand(model.cpf));

		return model;
	}

}
