package br.com.idogz.external.modelassembler.product;

import java.util.Objects;

import org.springframework.hateoas.RepresentationModel;

public class ProductModel extends RepresentationModel<ProductModel> {

	public Integer id;
	public Integer idCategory;
	public String name;
	public Double price;
	public String description;
	public Boolean active;
	public String urlImage;

	public ProductModel(final Integer id, final Integer idCategory, final String name, final Double price,
			final String description, final Boolean active, final String urlImage) {
		this.id = id;
		this.idCategory = idCategory;
		this.name = name;
		this.price = price;
		this.description = description;
		this.active = active;
		this.urlImage = urlImage;
	}

	@Override
	public int hashCode() {
		final var prime = 31;
		var result = super.hashCode();
		result = prime * result + Objects.hash(active, idCategory, description, id, name, price, urlImage);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj) || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (ProductModel) obj;
		return Objects.equals(active, other.active) && Objects.equals(idCategory, other.idCategory)
				&& Objects.equals(description, other.description) && Objects.equals(id, other.id)
				&& Objects.equals(name, other.name) && Objects.equals(price, other.price)
				&& Objects.equals(urlImage, other.urlImage);
	}

}
