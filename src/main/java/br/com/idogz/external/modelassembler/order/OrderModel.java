package br.com.idogz.external.modelassembler.order;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.springframework.hateoas.RepresentationModel;

import br.com.idogz.external.modelassembler.client.ClientModel;

public class OrderModel extends RepresentationModel<ClientModel> {

	public UUID id;
	public LocalDate date;
	public int ticketOrder;
	public String status;
	public String payment;
	public int clientId;
	public String notes;
	public List<OrderProductModel> orderProducts;
	public int paymentId;
	public String urlQrcode;

	public OrderModel(final UUID id, final LocalDate date, final int ticketOrder, final String status,
			final String payment,
			final int clientId, final String notes, final List<OrderProductModel> orderProducts, int paymentId, String urlQrCode) {
		this.id = id;
		this.date = date;
		this.ticketOrder = ticketOrder;
		this.status = status;
		this.clientId = clientId;
		this.notes = notes;
		this.orderProducts = orderProducts;
		this.payment = payment;
		this.paymentId = paymentId;
		this.urlQrcode = urlQrCode;
	}

	@Override
	public int hashCode() {
		final var PRIME = 31;
		final var result = super.hashCode();
		return PRIME * result + Objects.hash(clientId, date, id, notes, orderProducts, status, ticketOrder, payment);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj) || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (OrderModel) obj;
		return clientId == other.clientId && Objects.equals(date, other.date) && id == other.id
				&& Objects.equals(notes, other.notes) && Objects.equals(orderProducts, other.orderProducts)
				&& Objects.equals(status, other.status) && ticketOrder == other.ticketOrder && payment == other.payment;
	}

}
