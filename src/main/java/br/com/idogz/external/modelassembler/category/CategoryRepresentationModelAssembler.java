package br.com.idogz.external.modelassembler.category;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.idogz.adapter.controller.category.impl.CategoryControllerImpl;
import br.com.idogz.adapter.presenter.category.CategoryPresenter;
import br.com.idogz.domain.entity.Category;

@Component
public class CategoryRepresentationModelAssembler extends RepresentationModelAssemblerSupport<Category, CategoryModel> implements CategoryPresenter {
	
	public CategoryRepresentationModelAssembler() {
		super(CategoryControllerImpl.class, CategoryModel.class);
	}

	@Override
	public CategoryModel toModel(Category category) {
		final var model = new CategoryModel(category.id(), category.name());

		model.add(Link.of("/category/{name}").withRel(LinkRelation.of("category")).expand(model.name));

		return model;
	}
}
