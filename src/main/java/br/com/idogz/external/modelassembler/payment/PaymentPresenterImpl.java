package br.com.idogz.external.modelassembler.payment;

import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.idogz.adapter.presenter.payment.PaymentPresenter;
import br.com.idogz.domain.entity.Order;
import br.com.idogz.external.api.payment.PaymentApi;

@Component
public class PaymentPresenterImpl extends RepresentationModelAssemblerSupport<Order, PaymentModel>
implements PaymentPresenter {

	private final EntityLinks entityLinks;

	public PaymentPresenterImpl(final EntityLinks entityLinks) {
		super(PaymentApi.class, PaymentModel.class);
		this.entityLinks = entityLinks;
	}

	@Override
	public PaymentModel toModel(final Order order) {
		final var paymentModel = new PaymentModel(order.statusPayment().getDescription(), order.idPayment());
		paymentModel.add(entityLinks.linkToItemResource(PaymentModel.class, order.statusPayment().getId()));
		return paymentModel;
	}

}
