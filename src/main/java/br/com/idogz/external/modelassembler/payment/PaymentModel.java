package br.com.idogz.external.modelassembler.payment;

import java.util.Objects;

import org.springframework.hateoas.RepresentationModel;

public class PaymentModel extends RepresentationModel<PaymentModel> {

	public String status;
	public Integer idPayment;

	public PaymentModel(final String status, final Integer idPayment) {
		this.idPayment = idPayment;
		this.status = status;
	}

	@Override
	public int hashCode() {
		final var prime = 31;
		var result = super.hashCode();
		return prime * result + Objects.hash(idPayment, status);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj) || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (PaymentModel) obj;
		return Objects.equals(idPayment, other.idPayment) && Objects.equals(status, other.status);
	}


}
