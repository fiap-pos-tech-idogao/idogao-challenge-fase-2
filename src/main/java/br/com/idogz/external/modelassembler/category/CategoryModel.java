package br.com.idogz.external.modelassembler.category;

import java.util.Objects;

import org.springframework.hateoas.RepresentationModel;

public class CategoryModel extends RepresentationModel<CategoryModel> {

	public Integer id;
	public String name;

	public CategoryModel(final Integer id, final String name) {
		this.id = id;
		this.name = name;
	}

	@Override
	public int hashCode() {
		final var PRIME = 31;
		var result = super.hashCode();
		result = PRIME * result + Objects.hash(id, name);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj) || getClass() != obj.getClass()) {
			return false;
		}
		final var other = (CategoryModel) obj;
		return Objects.equals(id, other.id) && Objects.equals(name, other.name);
	}

}
