package br.com.idogz.external.modelassembler.product;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.idogz.adapter.controller.product.ProductController;
import br.com.idogz.adapter.presenter.product.ProductPresenter;
import br.com.idogz.domain.entity.Product;

@Component
public class ProductRepresentationModelAssembler extends RepresentationModelAssemblerSupport<Product, ProductModel>  implements ProductPresenter {

	public ProductRepresentationModelAssembler() {
		super(ProductController.class, ProductModel.class);
	}

	@Override
	public ProductModel toModel(final Product product) {
		final var model = new ProductModel(product.id(), product.category().id(), product.name().value(),
				product.price(),
				product.description(), product.active(), product.urlImage());

		model.add(Link.of("/product/{name}").withRel(LinkRelation.of("product")).expand(model.name));

		return model;
	}

	@Override
	public List<ProductModel> toModelList(final List<Product> products) {
		final List<ProductModel> result = new ArrayList<>();
		for (final Product product : products) {
			result.add(toModel(product));
		}
		return result;
	}
}
