package br.com.idogz.external.httpInterceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import br.com.idogz.external.metrics.ApiConsumingMetricCounter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class ApiRequestCounterInterceptor implements HandlerInterceptor {

	private final ApiConsumingMetricCounter counter;

	public ApiRequestCounterInterceptor(final ApiConsumingMetricCounter counter) {
		this.counter = counter;
	}

	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
			throws Exception {
		counter.increment();
		return true;
	}

}
