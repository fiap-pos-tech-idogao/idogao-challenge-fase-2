package br.com.idogz.external.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import br.com.idogz.external.httpInterceptor.ApiRequestCounterInterceptor;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	private final ApiRequestCounterInterceptor counterInterceptor;

	public WebConfig(final ApiRequestCounterInterceptor counterInterceptor) {
		this.counterInterceptor = counterInterceptor;
	}

	@Override
	public void addInterceptors(final @NonNull InterceptorRegistry registry) {
		registry.addInterceptor(counterInterceptor).excludePathPatterns("/api-docs/**", "/swagger-ui/**", "/actuator/**", "/error/**", "/error");
	}
}
