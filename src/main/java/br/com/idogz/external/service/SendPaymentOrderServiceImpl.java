package br.com.idogz.external.service;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.idogz.ApiMercadoPagoPropertiesFactory;
import br.com.idogz.adapter.repository.payment.ConfirmPaymentDataDto;
import br.com.idogz.adapter.repository.payment.CreatePaymentDataDto;
import br.com.idogz.adapter.service.SendPaymentOrderService;
import br.com.idogz.domain.entity.Order;

@Component
public class SendPaymentOrderServiceImpl implements SendPaymentOrderService {

    @Autowired
    private ApiMercadoPagoPropertiesFactory propertiesMercadoPago;

    public SendPaymentOrderServiceImpl() {
    }

    @Override
    public ConfirmPaymentDataDto sendPayment(final CreatePaymentDataDto dto, final Order order) {

        System.out.println("SENDING MERCADO PAGO ====>>>> [Url=" + propertiesMercadoPago.urlCreatePayment() + ", Body=" + getBody(dto) + "]");

        try {
            final ResponseEntity<ConfirmPaymentDataDto> response = new RestTemplate().postForEntity(
                    propertiesMercadoPago.urlCreatePayment(),
                    new HttpEntity<>(getBody(dto), getHeaders(order)), ConfirmPaymentDataDto.class);

            System.out.println("SUCCESS SENDING MERCADO PAGO ====>>>> [" + response.getBody().toString() + "]");

            return response.getBody();

        } catch (final RestClientException e) {
            System.out.println("ERROR SENDING MERCADO PAGO ====>>>> [" + e + "]");
            throw e;
        }
    }

    private HttpHeaders getHeaders(final Order order) {

        final var headers = new HttpHeaders();

        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.add("Authorization", MessageFormat.format("Bearer {0}", propertiesMercadoPago.accessToken()));
        headers.add("X-Idempotency-Key", order.id().toString());

        return headers;
    }

    private String getBody(final CreatePaymentDataDto dto) {

        var jsonData = "";
        try {
            final var objectMapper = new ObjectMapper();
            jsonData = objectMapper.writeValueAsString(dto);
        } catch (final JsonProcessingException ex) {
        }

        return jsonData;
    }
}
