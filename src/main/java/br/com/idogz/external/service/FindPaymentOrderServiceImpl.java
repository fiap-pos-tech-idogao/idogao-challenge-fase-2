package br.com.idogz.external.service;

import java.text.MessageFormat;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.com.idogz.ApiMercadoPagoPropertiesFactory;
import br.com.idogz.adapter.repository.payment.FindPaymentDataDto;
import br.com.idogz.adapter.service.FindPaymentOrderService;

@Component
public class FindPaymentOrderServiceImpl implements FindPaymentOrderService {

	private final ApiMercadoPagoPropertiesFactory propertiesMercadoPago;

	public FindPaymentOrderServiceImpl(final ApiMercadoPagoPropertiesFactory propertiesMercadoPago) {
		this.propertiesMercadoPago = propertiesMercadoPago;
	}

	@Override
	public FindPaymentDataDto findPayment(final String idPayment) {
			final ResponseEntity<FindPaymentDataDto> response = new RestTemplate().exchange(
					propertiesMercadoPago.urlFindPayment(), HttpMethod.GET, new HttpEntity<>(getHeaders()),
					FindPaymentDataDto.class, Map.of("idPayment", idPayment));
			return response.getBody();
	}

	private HttpHeaders getHeaders() {

		final var headers = new HttpHeaders();

		headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		headers.add("Authorization", MessageFormat.format("Bearer {0}", propertiesMercadoPago.accessToken()));

		return headers;
	}
}
