package br.com.idogz.external.metrics;

import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;

@Component
public class ApiConsumingMetricCounter {

	private final Counter counter;

	public ApiConsumingMetricCounter(final MeterRegistry meterRegistry) {
		counter = Counter.builder("idogz_api_request_counter")
				.description("Counts the number of requests made to the APIs")
				.tags("method", "GET, PUT, POST, DELETE")
				.register(meterRegistry);
	}

	public void increment() {
		counter.increment();
	}

}
