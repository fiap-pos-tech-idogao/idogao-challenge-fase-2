package br.com.idogz.external.api.product;

import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.idogz.adapter.controller.product.CreateProductDto;
import br.com.idogz.adapter.controller.product.ProductController;
import br.com.idogz.adapter.controller.product.UpdateProductDto;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.external.api.error.ApiError;
import br.com.idogz.external.modelassembler.client.ClientModel;
import br.com.idogz.external.modelassembler.product.ProductModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Product", description = "Product Management API")
@RestController
@RequestMapping(path = "/product")
@ExposesResourceFor(ProductModel.class)
public class ProductApi {

	private final ProductController controller;

	public ProductApi(final ProductController controller) {
		this.controller = controller;
	}

	@GetMapping("/category/{idCategory}")
	@Operation(summary = "Search for a Product by its idCategory", description = "Get the Product information by specifying the Category internal ID. The response is a object with: Name, Description, Price, Active status, Image URL, the Category internal ID and the Product internal ID.", tags = {
	"get" })
	@Parameter(name = "idCategory", description = "Product's by Category internal ID", schema = @Schema(description = "Product's by Category internal ID", example = "1", type = "integer", maxLength = 2147483647))
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Product found", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ProductModel.class)) }),
			@ApiResponse(responseCode = "400", description = "Invalid idCategory for Product provided", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
			@ApiResponse(responseCode = "404", description = "Product not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))) })
	public ResponseEntity<Object> findByCategoryId(@PathVariable final Integer idCategory) throws DomainException {
		final var model = controller.findProductByCategory(idCategory);
		return ResponseEntity.ok(model);
	}

	@PostMapping()
	@Operation(summary = "Create a new Product", description = "Create a new Product record with the data provided in the request.", tags = {
	"post" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Product created", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ClientModel.class))),
			@ApiResponse(responseCode = "400", description = "Product creation failed", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))) })
	public ResponseEntity<Object> create(@RequestBody final CreateProductRequest request) throws DomainException {
		final var createProductDto = new CreateProductDto(request.id(), request.idCategory(),
				request.name(),
				request.price(), request.description(), request.active(), request.urlImage());
		final var model = controller.create(createProductDto);
		Integer id = 0;
		if (model instanceof final ProductModel productModel) {
			id = productModel.id;
		}
		return ResponseEntity
				.created(ServletUriComponentsBuilder.fromCurrentRequest().path("").buildAndExpand(id).toUri())
				.body(model);
	}

	@PutMapping()
	@Operation(summary = "Update data for an existing Product", description = "Update an existing Product record with the data provided in the request.", tags = {
	"put" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Product update", content = @Content(mediaType = "application/json")),
			@ApiResponse(responseCode = "400", description = "Product updated failed", content = @Content(mediaType = "application/json")) })
	public void update(@RequestBody final UpdateProductRequest request) throws DomainException {
		final var updateProductDto = new UpdateProductDto(request.id(), request.idCategory(),
				request.name(),
				request.price(), request.description(), request.active(), request.urlImage());
		controller.update(updateProductDto);
		ResponseEntity.ok(true);
	}

	@DeleteMapping("/{id}")
	@Operation(summary = "Remove data from an existing Product", description = "Deactivate an existing Product with the data provided in the request.", tags = {
	"delete" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Product remove", content = @Content(mediaType = "application/json")),
			@ApiResponse(responseCode = "400", description = "Product remove failed", content = @Content(mediaType = "application/json")) })
	public void remove(@PathVariable final Integer id) throws DomainException {
		controller.remove(id);
		ResponseEntity.ok(true);
	}

}
