package br.com.idogz.external.api.order;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Order Update Information")
public record UpdateOrderStatusRequest(
		@Schema(description = "Status code to update the order", example = "2")
		int statusOrder) {

}
