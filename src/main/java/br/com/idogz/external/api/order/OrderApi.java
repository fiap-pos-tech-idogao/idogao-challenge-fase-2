package br.com.idogz.external.api.order;

import java.util.UUID;

import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.idogz.adapter.controller.order.CreateOrderRequestDto;
import br.com.idogz.adapter.controller.order.OrderController;
import br.com.idogz.adapter.controller.order.UpdateOrderStatusDto;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.external.api.error.ApiError;
import br.com.idogz.external.modelassembler.order.OrderModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Order", description = "Order Management API")
@RestController
@RequestMapping(path = "/order")
@ExposesResourceFor(OrderModel.class)
public class OrderApi {

	private final OrderController controller;

	public OrderApi(final OrderController controller) {
		this.controller = controller;
	}

	@GetMapping
	@Operation(summary = "Search all order information.",
	description = "Get information for all orders. The response is a list of objects with data about the Order.",
	tags = { "get" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Orders founds", content = @Content(mediaType = "application/json", schema = @Schema(implementation = OrderModel.class))) })
	public ResponseEntity<Object> list() throws DomainException {
		final var orders = controller.listAllOrders();
		return ResponseEntity.ok(orders);
	}

	@PostMapping
	@Operation(summary = "Create a new Order",
	description = "Save information related to a Client's new Order",
	tags = { "post" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Order created", content = @Content(mediaType = "application/json", schema = @Schema(implementation = OrderModel.class))),
			@ApiResponse(responseCode = "400", description = "Order creation failed", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))) })
	public ResponseEntity<Object> create(@RequestBody final CreateOrderRequest request) throws DomainException {
		final var orderProductList = request.orderProducts().stream()
				.map(orderProduct -> new CreateOrderRequestDto.OrderProductRequestDto(orderProduct.idProduct(),
						orderProduct.quantity(),
						orderProduct.notes()))
				.toList();
		final var orderDto = new CreateOrderRequestDto(request.ticketOrder(), request.date(), request.clientId(),
				request.notes(), orderProductList);
		final var model = controller.create(orderDto);
		return ResponseEntity.status(HttpStatus.CREATED).body(model);
	}

	@PatchMapping("/{id}")
	@Operation(summary = "Update the status from a ordem",
	description = "Update the status from a order with the given ID",
	tags = { "patch" })
	@Parameter(name = "id",
	description = "Order's by Order internal ID",
	schema = @Schema(description = "Unique internal order ID", example = "1"))
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Order status updated"),
			@ApiResponse(responseCode = "400", description = "Order status updated failed") })
	public ResponseEntity<Object> updateStatus(@PathVariable final UUID id,
			@RequestBody final UpdateOrderStatusRequest request) throws DomainException {
		final var newOrderStatus = new UpdateOrderStatusDto(id, request.statusOrder());
		controller.update(newOrderStatus);
		return ResponseEntity.ok().build();
	}

}
