package br.com.idogz.external.api.category;

import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.idogz.adapter.controller.category.CategoryController;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.external.api.error.ApiError;
import br.com.idogz.external.modelassembler.category.CategoryModel;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Category", description = "Category Management API")
@RestController
@RequestMapping(path = "/category")
@ExposesResourceFor(CategoryModel.class)
@Hidden
public class CategoryApi {
	private final CategoryController controller;

	public CategoryApi(final CategoryController controller) {
		this.controller = controller;
	}

	@GetMapping("/{id}")
	@Operation(summary = "Search for a Category by its idCategory", description = "Get the Category information by specifying the Category internal ID. The response is an object with: Name, Category internal ID and Product internal ID.", tags = {
	"get" })
	@Parameter(name = "id", description = "Category by Category internal ID", schema = @Schema(description = "Category by Category internal ID", example = "1", maxLength = 2_147_483_647, type = "integer"))
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Category found", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = CategoryModel.class))
			}),
			@ApiResponse(responseCode = "400", description = "Invalid idCategory provided", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
			@ApiResponse(responseCode = "404", description = "Category not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))
	})
	public ResponseEntity<Object> findById(@PathVariable final Integer id) throws DomainException {
		final var model = controller.getCategoryWith(id);
		return ResponseEntity.ok(model);
	}
}
