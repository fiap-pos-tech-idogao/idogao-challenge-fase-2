package br.com.idogz.external.api.payment;

import java.util.UUID;

import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.idogz.adapter.controller.payment.PaymentController;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.external.api.error.ApiError;
import br.com.idogz.external.modelassembler.order.OrderModel;
import br.com.idogz.external.modelassembler.payment.PaymentModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Payment", description = "Payment Management API")
@RestController
@RequestMapping(path = "/payment")
@ExposesResourceFor(PaymentModel.class)
public class PaymentApi {

	private final PaymentController controller;

	public PaymentApi(final PaymentController controller) {
		this.controller = controller;
	}

	@GetMapping("/{id}")
	@Operation(summary = "Search the payment status.", description = "Search the payment status for the given payment ID")
	@Parameter(name = "id", description = "Order's by Order internal ID", schema = @Schema(description = "Unique internal order ID", example = "1"))
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Status Found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = PaymentModel.class)))
	})
	public ResponseEntity<Object> verifyStatusPayment(@PathVariable final UUID id) throws DomainException {
		final var payment = controller.verifyStatusPayment(id);
		return ResponseEntity.ok(payment);
	}
}
