package br.com.idogz.external.api.product;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Product Model Information")
public record UpdateProductRequest(
		@Schema(description = "Product internal ID", example = "13")
		Integer id,

		@Schema(description = "Category internal ID", example = "1")
		Integer idCategory,

		@Schema(description = "Product's Name", example = "Cachorro-quente do iDogz")
		String name,

		@Schema(description = "Product's Price", example = "23,23")
		Double price,

		@Schema(description = "Products's Description", example = "Pão de hot-dog, Salsicha tradicional, Pure de batatas, Batata palha, Ketchup, Mostarda e Queijo ralado",
		minLength = 11, maxLength = 14, format = "\\d{3}.\\d{3}.\\d{3}-\\d{2}")
		String description,
		
		@Schema(description = "Product's Active", example = "True")
		Boolean active,

		@Schema(description = "Product's URL Image", example = "s3://idogz/product/snacks/cachorro_quente_idogz_new.png")
		String urlImage) {

}
