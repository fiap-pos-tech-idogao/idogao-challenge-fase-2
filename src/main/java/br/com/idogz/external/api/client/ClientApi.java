package br.com.idogz.external.api.client;

import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.idogz.adapter.controller.client.ClientController;
import br.com.idogz.adapter.controller.client.CreateClientRequestDto;
import br.com.idogz.domain.exception.DomainException;
import br.com.idogz.external.api.error.ApiError;
import br.com.idogz.external.modelassembler.client.ClientModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Client", description = "Client Management API")
@RestController
@RequestMapping(path = "/client")
@ExposesResourceFor(ClientModel.class)
public class ClientApi {

	private final ClientController controller;

	public ClientApi(final ClientController controller) {
		this.controller = controller;
	}

	@GetMapping("/{cpf}")
	@Operation(summary = "Search for a Client by its CPF",
	description = "Get the Client's information by specifying the CPF. The response is a object with: Name, CPF, Email and Client internal ID.",
	tags = { "get" })
	@Parameter(name = "cpf",
	description = "Client's by CPF",
	schema = @Schema(
			description = "Client's by CPF",
			example = "712.955.208-50",
			minLength = 11,
			maxLength = 14,
			format = "\\d{3}.\\d{3}.\\d{3}-\\d{2}"
			)
			)
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200",
					description = "Client's found",
					content = {
							@Content(
									mediaType = "application/json",
									schema = @Schema(implementation = ClientModel.class)
									)
			}
					),
			@ApiResponse(responseCode = "400", description = "Invalid CPF provided", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))),
			@ApiResponse(responseCode = "404", description = "Client's not found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))
	})
	public ResponseEntity<Object> find(@PathVariable final String cpf) throws DomainException {
		final var client = controller.findClientWith(cpf);
		return ResponseEntity.ok(client);
	}

	@PostMapping()
	@Operation(summary = "Saves a new Client data",
	description = "Create a new Customer record with the data provided in the request.",
	tags = { "post" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Client created", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ClientModel.class))),
			@ApiResponse(responseCode = "400", description = "Client creation failed", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class)))
	})
	public ResponseEntity<Object> create(@RequestBody final CreateClientRequest request) throws DomainException {
		final var clientDto = new CreateClientRequestDto(request.id(), request.name(), request.cpf(), request.email());
		final var client = controller.create(clientDto);
		var cpf = "";
		if(client instanceof final ClientModel clientModel) {
			cpf = clientModel.cpf;
		}
		return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequest().path("/{cpf}").buildAndExpand(cpf).toUri()).body(client);
	}

}
