package br.com.idogz;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.PlatformTransactionManager;

import br.com.idogz.adapter.controller.order.OrderController;
import br.com.idogz.adapter.controller.order.impl.OrderControllerImpl;
import br.com.idogz.adapter.gateway.order.impl.FindAllOrderDataGatewayImpl;
import br.com.idogz.adapter.gateway.order.impl.FindOrderByIdDataGatewayImpl;
import br.com.idogz.adapter.gateway.order.impl.FindOrderByIdPaymentDataGatewayImpl;
import br.com.idogz.adapter.gateway.order.impl.SaveOrderDataGatewayImpl;
import br.com.idogz.adapter.gateway.order.impl.UpdateOrderStatusDataGatewayImpl;
import br.com.idogz.adapter.gateway.payment.impl.SendPaymentOrderDataGatewayImpl;
import br.com.idogz.adapter.gateway.order.impl.SaveOrderProductDataGatewayImpl;
import br.com.idogz.adapter.presenter.order.OrderPresenter;
import br.com.idogz.adapter.repository.order.OrderProductRepository;
import br.com.idogz.adapter.repository.order.OrderRepository;
import br.com.idogz.adapter.service.SendPaymentOrderService;
import br.com.idogz.domain.factory.order.OrderBuilder;
import br.com.idogz.domain.factory.order.OrderBuilderImpl;
import br.com.idogz.domain.factory.order.OrderProductFactory;
import br.com.idogz.domain.factory.order.OrderProductFactoryImpl;
import br.com.idogz.domain.gateway.client.FindClientDataGateway;
import br.com.idogz.domain.gateway.order.CreateOrderDataGateway;
import br.com.idogz.domain.gateway.order.FindAllOrdersDataGateway;
import br.com.idogz.domain.gateway.order.FindOrderByIdDataGateway;
import br.com.idogz.domain.gateway.order.FindOrderByIdPaymentDataGateway;
import br.com.idogz.domain.gateway.order.SaveOrderProductDataGateway;
import br.com.idogz.domain.gateway.order.UpdateOrderStatusDataGateway;
import br.com.idogz.domain.gateway.payment.SendPaymentOrderDataGateway;
import br.com.idogz.domain.gateway.product.FindProductDataGateway;
import br.com.idogz.domain.usecase.TransactionManagerHelper;
import br.com.idogz.domain.usecase.order.CreateOrderUseCaseInteractor;
import br.com.idogz.domain.usecase.order.ListOrdersUseCaseInteractor;
import br.com.idogz.domain.usecase.order.UpdateOrderStatusUseCaseInteractor;
import br.com.idogz.domain.usecase.order.impl.CreateOrderUseCaseInteractorImpl;
import br.com.idogz.domain.usecase.order.impl.ListOrdersUseCaseInteractorImpl;
import br.com.idogz.domain.usecase.order.impl.UpdateOrderStatusUseCaseInteractorImpl;
import br.com.idogz.external.TransactionManagerHelperImpl;
import br.com.idogz.external.repository.order.OrderJpaRepository;
import br.com.idogz.external.repository.order.OrderProductJpaRepository;
import br.com.idogz.external.repository.order.OrderProductRepositoryImpl;
import br.com.idogz.external.repository.order.OrderRepositoryImpl;

@Configuration
public class OrderContextConfiguration {

	@Bean
	OrderProductRepository orderProductRepository(final OrderProductJpaRepository repository) {
		return new OrderProductRepositoryImpl(repository);
	}

	@Bean
	OrderRepository orderRepository(final OrderJpaRepository repository) {
		return new OrderRepositoryImpl(repository);
	}

	@Bean
	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	TransactionManagerHelper transactionManagerHelper(final PlatformTransactionManager transactionManager) {
		return new TransactionManagerHelperImpl(transactionManager);
	}

	@Bean
	ListOrdersUseCaseInteractor listAllOrdersUseCaseInteractor(final FindAllOrdersDataGateway gateway) {
		return new ListOrdersUseCaseInteractorImpl(gateway);
	}

	@Bean
	FindAllOrdersDataGateway findOrderDataGateway(final OrderBuilder builder, final OrderRepository repository) {
		return new FindAllOrderDataGatewayImpl(repository, builder);
	}

	@Bean
	FindOrderByIdDataGateway findOrderByIdDataGateway(final OrderRepository repository, final OrderBuilder builder) {
		return new FindOrderByIdDataGatewayImpl(repository, builder);
	}

	@Bean
	FindOrderByIdPaymentDataGateway findOrderByIdPaymentDataGateway(final OrderRepository repository, final OrderBuilder builder) {
		return new FindOrderByIdPaymentDataGatewayImpl(repository, builder);
	}

	@Bean
	OrderController orderController(final CreateOrderUseCaseInteractor orderService, final OrderBuilder builder, final OrderPresenter presenter, final UpdateOrderStatusUseCaseInteractor useCase, final ListOrdersUseCaseInteractor listAllOrders, final FindOrderByIdDataGateway findOrderByIdDataGateway) {
		return new OrderControllerImpl(orderService, builder, presenter, useCase, listAllOrders, findOrderByIdDataGateway);
	}

	@Bean
	OrderProductFactory orderProductFactory(final FindProductDataGateway gateway) {
		return new OrderProductFactoryImpl(gateway);
	}

	@Bean
	OrderBuilder orderBuilder(final FindClientDataGateway dataGateway, final OrderProductFactory orderProductFactory) {
		return new OrderBuilderImpl(dataGateway, orderProductFactory);
	}

	@Bean
	CreateOrderDataGateway saveOrderDataGateway(final OrderRepository orderRepository) {
		return new SaveOrderDataGatewayImpl(orderRepository);
	}

	@Bean
	SaveOrderProductDataGateway saveOrderProductDataGateway(final OrderProductRepository repository) {
		return new SaveOrderProductDataGatewayImpl(repository);
	}

	@Bean
	SendPaymentOrderDataGateway sendPaymentOrderDataGateway(final SendPaymentOrderService service) {
		return new SendPaymentOrderDataGatewayImpl(service);
	}

	@Bean
	CreateOrderUseCaseInteractor createOrderUseCaseInteractor(final CreateOrderDataGateway createOrderGateway,
			final OrderProductRepository orderProductRepository, final TransactionManagerHelper transactionManagerHelper, final SaveOrderProductDataGateway saveOrderProductGateway, final SendPaymentOrderDataGateway sendPaymentGateway) {
		return new CreateOrderUseCaseInteractorImpl(createOrderGateway, transactionManagerHelper, saveOrderProductGateway, sendPaymentGateway);
	}

	@Bean
	UpdateOrderStatusDataGateway updateOrderStatusDataGateway(final OrderRepository repository) {
		return new UpdateOrderStatusDataGatewayImpl(repository);
	}

	@Bean
	UpdateOrderStatusUseCaseInteractor updateOrderStatusUseCase(final UpdateOrderStatusDataGateway gateway, final TransactionManagerHelper transactionManagerHelper) {
		return new UpdateOrderStatusUseCaseInteractorImpl(gateway, transactionManagerHelper);
	}

}
