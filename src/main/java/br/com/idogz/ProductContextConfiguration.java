package br.com.idogz;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.idogz.adapter.controller.product.ProductController;
import br.com.idogz.adapter.controller.product.impl.ProductControllerImpl;
import br.com.idogz.adapter.gateway.product.impl.CreateProductGatewayImpl;
import br.com.idogz.adapter.gateway.product.impl.DeleteProductGatewayImpl;
import br.com.idogz.adapter.gateway.product.impl.FindProductGatewayImpl;
import br.com.idogz.adapter.gateway.product.impl.UpdateProductGatewayImpl;
import br.com.idogz.adapter.presenter.product.ProductPresenter;
import br.com.idogz.adapter.repository.product.ProductRepository;
import br.com.idogz.domain.factory.product.ProductFactory;
import br.com.idogz.domain.factory.product.impl.ProductFactoryImpl;
import br.com.idogz.domain.gateway.product.CreateProductDataGateway;
import br.com.idogz.domain.gateway.product.DeleteProductDataGateway;
import br.com.idogz.domain.gateway.product.FindProductDataGateway;
import br.com.idogz.domain.gateway.product.UpdateProductDataGateway;
import br.com.idogz.domain.usecase.category.FindCategoryByIdUseCaseInteractor;
import br.com.idogz.domain.usecase.product.CreateProductUseCaseInteractor;
import br.com.idogz.domain.usecase.product.DeleteProductUseCaseInteractor;
import br.com.idogz.domain.usecase.product.FindProductsByCategoryIdUseCaseInteractor;
import br.com.idogz.domain.usecase.product.UpdateProductUseCaseInteractor;
import br.com.idogz.domain.usecase.product.impl.CreateProductUseCaseInteractorImpl;
import br.com.idogz.domain.usecase.product.impl.DeleteProductUseCaseInteractorImpl;
import br.com.idogz.domain.usecase.product.impl.FindProductsByCategoryIdUseCaseInteractorImpl;
import br.com.idogz.domain.usecase.product.impl.UpdateProductUseCaseInteractorImpl;
import br.com.idogz.external.repository.product.ProductJpaRepository;
import br.com.idogz.external.repository.product.ProductRepositoryImpl;

@Configuration
public class ProductContextConfiguration {

	@Bean
	ProductRepository productRepository(final ProductJpaRepository repository) {
		return new ProductRepositoryImpl(repository);
	}

	@Bean
	ProductFactory productFactory(final FindCategoryByIdUseCaseInteractor findCategory) {
		return new ProductFactoryImpl(findCategory);
	}

	@Bean
	CreateProductDataGateway createProductDataGateway(final ProductRepository repository, final ProductFactory factory) {
		return new CreateProductGatewayImpl(repository, factory);
	}

	@Bean
	UpdateProductDataGateway updateProductDataGateway(final ProductRepository repository) {
		return new UpdateProductGatewayImpl(repository);
	}

	@Bean
	DeleteProductDataGateway deleteProductDataGateway(final ProductRepository repository) {
		return new DeleteProductGatewayImpl(repository);
	}

	@Bean
	FindProductDataGateway findProductDataGateway(final ProductFactory factory, final ProductRepository repository) {
		return new FindProductGatewayImpl(repository, factory);
	}

	@Bean
	CreateProductUseCaseInteractor createProductUseCaseInteractor(final CreateProductDataGateway createProductGateway,
			final FindProductDataGateway findProductDataGateway) {
		return new CreateProductUseCaseInteractorImpl(createProductGateway, findProductDataGateway);
	}

	@Bean
	UpdateProductUseCaseInteractor updateProductUseCaseInteractor(final UpdateProductDataGateway updateProductGateway,
			final FindProductDataGateway findProductDataGateway) {
		return new UpdateProductUseCaseInteractorImpl(updateProductGateway, findProductDataGateway);
	}

	@Bean
	DeleteProductUseCaseInteractor deleteProductUseCaseInteractor(final FindProductDataGateway findProductGateway,
			final DeleteProductDataGateway deleteProductGateway) {
		return new DeleteProductUseCaseInteractorImpl(deleteProductGateway, findProductGateway);
	}

	@Bean
	FindProductsByCategoryIdUseCaseInteractor findProductsByCategoryId(final FindProductDataGateway gateway) {
		return new FindProductsByCategoryIdUseCaseInteractorImpl(gateway);
	}

	@Bean
	ProductController productController(final CreateProductUseCaseInteractor createProductUseCase,
			final UpdateProductUseCaseInteractor updateProductUseCase,
			final DeleteProductUseCaseInteractor deleteProductUseCase,
			final ProductPresenter presenter,
			final ProductFactory factory, final FindProductsByCategoryIdUseCaseInteractor findProductsByCategoryId) {
		return new ProductControllerImpl(createProductUseCase, updateProductUseCase, deleteProductUseCase, presenter,
				factory, findProductsByCategoryId);
	}

}
