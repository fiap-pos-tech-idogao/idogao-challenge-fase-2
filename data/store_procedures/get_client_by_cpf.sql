CREATE OR REPLACE FUNCTION get_client_by_cpf(
    _cpf CHAR(11)
)
RETURNS TABLE(idClient INTEGER, name VARCHAR, email VARCHAR, cpf CHAR(11))
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY
    SELECT t.idClient, t.name, t.email, t.cpf
    FROM tbClient t
    WHERE t.cpf = _cpf;
END;
$$;