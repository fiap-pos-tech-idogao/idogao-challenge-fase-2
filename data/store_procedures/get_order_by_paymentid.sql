--DROP FUNCTION public.get_category_by_id(integer);

CREATE OR REPLACE FUNCTION public.get_order_by_paymentid(_paymentid integer)
 RETURNS TABLE(idorder uuid, ticketorder integer, dateorder timestamp, totalvalueorder float8, idstatus integer, idclient integer, notesorder character varying, idstatuspayment integer, idpayment integer)
 LANGUAGE plpgsql
AS $function$
BEGIN
    RETURN QUERY
    SELECT t.idorder, t.ticketorder, t.dateorder, t.totalvalueorder, t.idstatus, t.idclient, t.notesorder, t.idstatuspayment, t.idpayment
    FROM tborder t
    WHERE t.idpayment = _paymentid;
END;
$function$
;