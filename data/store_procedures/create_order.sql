CREATE OR REPLACE FUNCTION create_order(
	_idOrder uuid,
    _dateorder DATE,
    _totalvalueorder DOUBLE PRECISION, -- Ajuste conforme necessário baseado no tipo real da coluna
    _idstatus INTEGER,
    _idstatuspayment INTEGER,
    _idclient INTEGER,
    _notesorder VARCHAR(100),
    _idpayment INTEGER
)
RETURNS TABLE(ticketorder INTEGER)
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY
    INSERT INTO tborder (idorder, ticketorder, dateorder, totalvalueorder, idstatus, idclient, notesorder, idstatuspayment, idpayment)
    VALUES (_idOrder, NEXTVAL(pg_get_serial_sequence('tborder','ticketorder')), _dateorder, _totalvalueorder, _idstatus, _idclient, _notesorder, _idstatuspayment, _idpayment)
    RETURNING tborder.ticketorder;
END;
$$;