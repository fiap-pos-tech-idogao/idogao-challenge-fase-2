-- DROP FUNCTION public.get_product_by_name(varchar);

CREATE OR REPLACE FUNCTION public.get_product_by_name(_name character varying)
 RETURNS TABLE(idproduct integer, idcategory integer, name character varying, priceproduct float8, description character varying, active bool, urlimage character varying)
 LANGUAGE plpgsql
AS $function$
BEGIN
    RETURN QUERY
    SELECT t.idproduct, t.idcategory, t.name, t.priceproduct, t.description, t.active, t.urlimage
    FROM tbProduct t
    WHERE t.name = _name;
END;
$function$
;