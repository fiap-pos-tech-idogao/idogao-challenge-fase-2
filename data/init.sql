CREATE TABLE tbClient
(
 idClient SERIAL PRIMARY KEY,
 name VARCHAR(200) NOT NULL,
 email VARCHAR(100) NOT NULL,
 cpf CHAR(11) NOT NULL,
 UNIQUE (cpf)
);

CREATE TABLE tbOrder
(
 idOrder UUID PRIMARY KEY,
 ticketOrder SERIAL NOT NULL,
 dateOrder TIMESTAMP NOT NULL,
 totalValueOrder FLOAT NOT NULL,
 idStatus INT NOT NULL,
 idClient INT NOT NULL,
 notesOrder VARCHAR(100),
 idStatusPayment INT NOT NULL,
 idPayment INT NOT NULL
);

CREATE TABLE tbStatusOrder
(
 idStatus SERIAL PRIMARY KEY,
 description VARCHAR(100) NOT NULL,
 UNIQUE (description)
);

CREATE TABLE tbStatusPayment
(
 idStatus SERIAL PRIMARY KEY,
 description VARCHAR(100) NOT NULL
);

CREATE TABLE tbOrderProduct
(
 idOrder UUID NOT NULL,
 idProduct INT NOT NULL,
 quantity INT NOT NULL,
 notes VARCHAR(200),
 PRIMARY KEY(idOrder, idProduct)
);

CREATE TABLE tbProduct
(
 idProduct SERIAL PRIMARY KEY,
 name VARCHAR(100) NOT NULL,
 priceProduct FLOAT NOT NULL,
 description VARCHAR(200) NOT NULL,
 idCategory INT NOT NULL,
 urlImage VARCHAR(500),
 active BOOLEAN NOT NULL DEFAULT True,
 UNIQUE (name, description)
);

CREATE TABLE tbCategory
(
 idCategory SERIAL PRIMARY KEY,
 nameCategory VARCHAR(50) NOT NULL,
 UNIQUE (nameCategory)
);

-- Criação da tabela de pagamento
CREATE TABLE tbPayment
(
 idPayment SERIAL PRIMARY KEY,
 paymentType VARCHAR(50) NOT NULL
);

-- Adição de chaves estrangeiras
ALTER TABLE tbOrder 
    ADD FOREIGN KEY(idStatusPayment) REFERENCES tbStatusPayment(idStatus),
    ADD FOREIGN KEY(idStatus) REFERENCES tbStatusOrder(idStatus),
    ADD FOREIGN KEY(idClient) REFERENCES tbClient(idClient);

ALTER TABLE tbOrderProduct 
    ADD FOREIGN KEY(idOrder) REFERENCES tbOrder(idOrder),
    ADD FOREIGN KEY(idProduct) REFERENCES tbProduct(idProduct);

ALTER TABLE tbProduct 
    ADD FOREIGN KEY(idCategory) REFERENCES tbCategory(idCategory);

-- Criação de Categorias
INSERT INTO tbCategory (nameCategory) VALUES
    ('Lanche'),
    ('Acompanhamento'),
    ('Bebida'),
    ('Sobremesa');

-- Criação de Status do Pedido
INSERT INTO tbStatusOrder (description) VALUES
    ('Aguardando Pagamento'),
    ('Recebido'),
    ('Em Preparação'),
    ('Pronto'),
    ('Finalizado');

-- Criação de Status do Pagamento
INSERT INTO tbStatusPayment (description) VALUES
    ('Pending'),
    ('Confirmed'),
    ('Refused');

-- Criação dos Itens da Categoria Lanches
INSERT INTO tbProduct (name, priceProduct, description, idCategory, urlImage, active) VALUES 
    ('Cachorro-quente Clássico', 7.99, 'Pão de hot dog, Salsicha tradicional, Ketchup e mostarda', 1, 's3://idogz/products/snacks/cachorro_quente_classico.png', True),
    ('Cachorro-quente Especial', 9.99, 'Pão de hot dog, Salsicha de frango, Maionese verde e batata palha', 1, 's3://idogz/products/snacks/cachorro_quente_especial.png', True),
    ('Cachorro-quente Vegano', 788.99, 'Pão de hot dog integral, Salsicha vegetal, Maionese vegana e cebola caramelizada', 1, 's3://idogz/products/snacks/cachorro_quente_vegano.png', True);

-- Criação dos Itens da Categoria Acompanhamentos
INSERT INTO tbProduct (name, priceProduct, description, idCategory, urlImage, active) VALUES 
    ('Batata Frita', 8.99, 'Pequena', 2, 's3://idogz/products/side_dishes/batata_frita.png', True),
    ('Onion Rings', 9.99, 'Pequena', 2, 's3://idogz/products/side_dishes/onio_rings.png', True),
    ('Nuggets de Frango', 10.99, '6 unidades', 2, 's3://idogz/products/side_dishes/nuggets_frango.png', True);

-- Criação dos Itens da Categoria Bebidas
INSERT INTO tbProduct (name, priceProduct, description, idCategory, urlImage, active) VALUES 
    ('Coca-Cola', 5.99, 'Coca Cola lata 350ml', 3, 's3://idogz/products/drinks/coca_cola.png', True),
    ('Suco Natural de Laranja', 6.99, 'Copo de 500ml', 3, 's3://idogz/products/drinks/suco_natural_laranja.png', True),
    ('Água Mineral', 3.99, 'Garrafa de 500ml', 3, 's3://idogz/products/drinks/agua_mineral.png', True);

-- Criação dos Itens da Categoria Sobremesas
INSERT INTO tbProduct (name, priceProduct, description, idCategory, urlImage, active) VALUES 
    ('Sundae de Chocolate', 12.99, 'Copo de 500ml', 4, 's3://idogz/products/desserts/sundae_chocolate.png', True),
    ('Cheesecake de Morango', 14.99, 'Unidade', 4, 's3://idogz/products/desserts/cheesecake_morango.png', True),
    ('Brownie com Sorvete', 11.99, 'Unidade', 4, 's3://idogz/products/desserts/brownie_sorvete.png', True);

-- Criação de um cliente Dummy
INSERT INTO tbClient (name, email, cpf) VALUES 
    ('iDoguetz', 'idogzdev@gmail.com', '73868683917'),
    ('Marcelo', 'marcelo@gmail.com', '07769032091'),
    ('Rodrigo', 'rodrigo@gmail.com', '28654989050'),
    ('Bruna', 'bruna@gmail.com', '32038173060'),
    ('Kleiton', 'kleiton@gmail.com', '87804924011');

-- Criando a extensão para o uuid_generate
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Criação de pedidos iniciais
INSERT INTO tbOrder (
    idOrder, 
    ticketOrder, 
    dateOrder, 
    totalValueOrder, 
    idStatus, 
    idClient, 
    notesOrder, 
    idStatusPayment, 
    idPayment
) VALUES
(
    uuid_generate_v4(), -- Gerar um UUID automaticamente
    1, -- ticketOrder
    '2024-07-26 12:30:00', -- dateOrder
    22.97, -- totalValueOrder
    2, -- idStatus
    1, -- idClient
    'Pedido urgente', -- notesOrder
    2, -- idStatusPayment
    1001 -- idPayment
),
(
    uuid_generate_v4(), -- Gerar um UUID automaticamente
    2, -- ticketOrder
    '2024-07-26 14:00:00', -- dateOrder
    37.97, -- totalValueOrder
    3, -- idStatus
    2, -- idClient
    'Entregar no escritório', -- notesOrder
    2, -- idStatusPayment
    1002 -- idPayment
),
(
    uuid_generate_v4(), -- Gerar um UUID automaticamente
    3, -- ticketOrder
    '2024-07-26 15:45:00', -- dateOrder
    16.98, -- totalValueOrder
    4, -- idStatus
    3, -- idClient
    'Sem cebola', -- notesOrder
    2, -- idStatusPayment
    1003 -- idPayment
),
(
    uuid_generate_v4(), -- Gerar um UUID automaticamente
    4, -- ticketOrder
    '2024-07-26 17:00:00', -- dateOrder
    25.97, -- totalValueOrder
    4, -- idStatus
    4, -- idClient
    'Adicionar molho extra', -- notesOrder
    2, -- idStatusPayment
    1004 -- idPayment
),
(
    uuid_generate_v4(), -- Gerar um UUID automaticamente
    5, -- ticketOrder
    '2024-07-26 18:30:00', -- dateOrder
    29.97, -- totalValueOrder
    1, -- idStatus
    5, -- idClient
    'Para levar', -- notesOrder
    2, -- idStatusPayment
    1005 -- idPayment
);

-- Inserir itens para o Pedido 1
INSERT INTO tbOrderProduct (idOrder, idProduct, quantity, notes) VALUES
    (
        (SELECT idOrder FROM tbOrder WHERE ticketOrder = 1 AND dateOrder = '2024-07-26 12:30:00'),
        1,
        2,
        'Pedido urgente - Cachorro-quente Clássico'
    ),
    (
        (SELECT idOrder FROM tbOrder WHERE ticketOrder = 1 AND dateOrder = '2024-07-26 12:30:00'),
        2,
        1,
        'Pedido urgente - Cachorro-quente Especial'
    );

-- Inserir itens para o Pedido 2
INSERT INTO tbOrderProduct (idOrder, idProduct, quantity, notes) VALUES
    (
        (SELECT idOrder FROM tbOrder WHERE ticketOrder = 2 AND dateOrder = '2024-07-26 14:00:00'),
        3,
        3,
        'Entregar no escritório - Coca-Cola'
    ),
    (
        (SELECT idOrder FROM tbOrder WHERE ticketOrder = 2 AND dateOrder = '2024-07-26 14:00:00'),
        1,
        1,
        'Entregar no escritório - Cachorro-quente Clássico'
    );

-- Inserir itens para o Pedido 3
INSERT INTO tbOrderProduct (idOrder, idProduct, quantity, notes) VALUES
    (
        (SELECT idOrder FROM tbOrder WHERE ticketOrder = 3 AND dateOrder = '2024-07-26 15:45:00'),
        2,
        1,
        'Sem cebola - Cachorro-quente Especial'
    ),
    (
        (SELECT idOrder FROM tbOrder WHERE ticketOrder = 3 AND dateOrder = '2024-07-26 15:45:00'),
        3,
        2,
        'Sem cebola - Coca-Cola'
    );

-- Inserir itens para o Pedido 4
INSERT INTO tbOrderProduct (idOrder, idProduct, quantity, notes) VALUES
    (
        (SELECT idOrder FROM tbOrder WHERE ticketOrder = 4 AND dateOrder = '2024-07-26 17:00:00'),
        1,
        1,
        'Adicionar molho extra - Cachorro-quente Clássico'
    ),
    (
        (SELECT idOrder FROM tbOrder WHERE ticketOrder = 4 AND dateOrder = '2024-07-26 17:00:00'),
        3,
        1,
        'Adicionar molho extra - Coca-Cola'
    );

-- Inserir itens para o Pedido 5
INSERT INTO tbOrderProduct (idOrder, idProduct, quantity, notes) VALUES
    (
        (SELECT idOrder FROM tbOrder WHERE ticketOrder = 5 AND dateOrder = '2024-07-26 18:30:00'),
        2,
        2,
        'Para levar - Cachorro-quente Especial'
    ),
    (
        (SELECT idOrder FROM tbOrder WHERE ticketOrder = 5 AND dateOrder = '2024-07-26 18:30:00'),
        3,
        1,
        'Para levar - Coca-Cola'
    );