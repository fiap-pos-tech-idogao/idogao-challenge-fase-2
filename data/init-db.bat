kubectl cp . idogz-db-0:/mnt/data
kubectl exec idogz-db-0 -- psql -U postgres -d postgres -a -f /mnt/data/init.sql
kubectl exec idogz-db-0 -- psql -U postgres -d postgres -a -f /mnt/data/store_procedures/create_order_product.sql
kubectl exec idogz-db-0 -- psql -U postgres -d postgres -a -f /mnt/data/store_procedures/create_order.sql
kubectl exec idogz-db-0 -- psql -U postgres -d postgres -a -f /mnt/data/store_procedures/get_category_by_id.sql
kubectl exec idogz-db-0 -- psql -U postgres -d postgres -a -f /mnt/data/store_procedures/get_client_by_cpf.sql
kubectl exec idogz-db-0 -- psql -U postgres -d postgres -a -f /mnt/data/store_procedures/get_product_by_categoryid.sql
kubectl exec idogz-db-0 -- psql -U postgres -d postgres -a -f /mnt/data/store_procedures/get_product_by_id.sql
kubectl exec idogz-db-0 -- psql -U postgres -d postgres -a -f /mnt/data/store_procedures/get_product_by_name.sql
kubectl exec idogz-db-0 -- psql -U postgres -d postgres -a -f /mnt/data/store_procedures/get_order_by_paymentid.sql
