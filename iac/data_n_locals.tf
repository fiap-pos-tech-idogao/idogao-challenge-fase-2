data "aws_caller_identity" "current" {}

data "aws_eks_cluster" "default" {
  name = aws_eks_cluster.eks_cluster.name
}

data "aws_eks_cluster_auth" "default" {
  name = aws_eks_cluster.eks_cluster.name
}

data "aws_availability_zones" "available" {
  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}

locals {
  cluster_name = "${var.name}-eks"
  eks_connection = {
    host                   = data.aws_eks_cluster.default.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.default.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.default.token
  }
}
